-- migrate:up

alter table if exists customer
drop column if exists time_zone;

drop table if exists timeline_task;
