-- migrate:up

do $$
begin
	if exists (select 1
			   from information_schema.columns
			   where table_schema = 'public'
			   and table_name = 'task'
			   and column_name = 'is_completed') then
        alter table task
        alter column is_completed set default false;
	end if;
end
$$
