-- migrate:up

-- Customers

create table if not exists planning_style (
    style text primary key
);

insert into planning_style
    (style)
values
    ('today'),
    ('tomorrow')
on conflict (style) do nothing;

create table if not exists customer (
    id serial primary key,
    planning_style text references planning_style (style)
);

-- Goals

create table if not exists color (
    id serial primary key,
    hue integer not null,
    saturation integer not null,
    lightness integer not null,
    unique (hue, saturation, lightness)
);

create table if not exists goal_state (
    state text primary key
);

insert into goal_state
    (state)
values
    ('active'),
    ('completed'),
    ('archived')
on conflict (state) do nothing;

create table if not exists goal (
    id serial primary key,
    customer_id integer references customer (id),
    text text not null,
    color_id integer references color (id),
    state text references goal_state (state),
    order_position integer not null
);

create table if not exists goal_snapshot (
    id serial primary key,
    customer_id integer references customer (id),
    date timestamptz default current_timestamp,
    snapshot jsonb not null
);

-- Tasks

create table if not exists task (
    id serial primary key,
    customer_id integer references customer (id),
    date timestamptz,
    text text not null,
    goal_id integer references goal (id),
    is_completed boolean not null,
    order_position integer not null
);

-- Questions

create table if not exists question (
    id serial primary key,
    planning_style text references planning_style (style),
    text text not null
);

create table if not exists customer_question (
    customer_id integer references customer (id),
    question_id integer references question (id)
);

-- Daily Reflections

create table if not exists daily_rating (
    rating text primary key
);

insert into daily_rating
    (rating)
values
    ('great'),
    ('good'),
    ('okay'),
    ('bad'),
    ('awful')
on conflict (rating) do nothing;

create table if not exists daily_reflection (
    id serial primary key,
    customer_id integer references customer (id),
    date timestamptz not null,
    rating text references daily_rating (rating),
    note text
);
