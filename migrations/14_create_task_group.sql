-- migrate:up

-- Create task_group table.
create table if not exists task_group (
    id serial primary key,
    customer_id integer references customer (id),
    date timestamptz not null,
    name text not null,
    day_order integer not null
);

-- Add group_id column to task table.
alter table if exists task
add column if not exists group_id integer references task_group (id);

-- Rename order_number column in task table to group_order.
do $$
begin
    if exists (select 1
               from information_schema.columns
               where table_schema = 'public'
               and table_name = 'task'
               and column_name = 'order_number') then
        alter table task
        rename column order_number to group_order;
    end if;
end
$$;
