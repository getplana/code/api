-- migrate:up

drop table if exists goal_state cascade;

alter table if exists goal
drop column if exists state;

alter table if exists goal
add column if not exists is_active boolean default true not null;
