-- migrate:up

create table if not exists timeline_task (
    id serial primary key,
    customer_id integer references customer (id),
    date timestamptz,
    text text not null,
    goal_text text not null,
    hue integer not null,
    saturation integer not null,
    lightness integer not null,
    is_completed boolean not null,
    order_number integer not null
);
