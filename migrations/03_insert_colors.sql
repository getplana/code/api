-- migrate:up

alter table if exists color
add column if not exists name text;

insert into color (
    hue,
    saturation,
    lightness,
    name
) values
    (0,   100, 40, 'Red'),
    (29,  87,  45, 'Orange'),
    (46,  99,  44, 'Yellow'),
    (132, 56,  31, 'Green'),
    (193, 92,  31, 'Teal'),
    (215, 75,  43, 'Blue'),
    (255, 53,  45, 'Purple'),
    (288, 54,  46, 'Magenta'),
    (0,   0,   36, 'Gray')
on conflict (
    hue,
    saturation,
    lightness
) do nothing;
