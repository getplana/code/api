-- migrate:up

-- Create goal_state table.

create table if not exists goal_state (
    state text primary key
);

insert into goal_state
    (state)
values
    ('active'),
    ('archived'),
    ('deleted')
on conflict (state) do nothing;

-- Change column in goal table.

alter table if exists goal
drop column if exists is_active;

alter table if exists goal
add column if not exists state text;

do $$
begin
    if not exists (select 1
                   from pg_constraint
                   where conname = 'goal_state_fkey') then
        alter table if exists goal 
        add constraint goal_state_fkey foreign key (state) references goal_state (state);
    end if;
end;
$$;