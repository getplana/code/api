-- migrate:up

do $$
begin
	if exists (select 1
			   from information_schema.columns
			   where table_schema = 'public'
			   and table_name = 'task'
			   and column_name = 'date') then
        alter table task
        alter column date type date;
	end if;
end
$$;

do $$
begin
	if exists (select 1
			   from information_schema.columns
			   where table_schema = 'public'
			   and table_name = 'timeline_task'
			   and column_name = 'date') then
        alter table timeline_task
        alter column date type date;
	end if;
end
$$;
