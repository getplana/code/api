-- migrate:up

alter table if exists goal
add column if not exists created_at timestamptz default current_timestamp;
