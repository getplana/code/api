-- migrate:up

do $$
begin
    if exists (select 1
               from information_schema.columns
               where table_schema = 'public'
               and table_name = 'task'
               and column_name = 'order_position') then
        alter table task
        rename column order_position to order_number;
    end if;
end
$$;

do $$
begin
    if exists (select 1
               from information_schema.columns
               where table_schema = 'public'
               and table_name = 'goal'
               and column_name = 'order_position') then
        alter table goal
        rename column order_position to order_number;
    end if;
end
$$;
