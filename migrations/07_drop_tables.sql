-- migrate:up

drop table if exists planning_style cascade;
drop table if exists question cascade;
drop table if exists customer_question;
drop table if exists daily_reflection;
drop table if exists daily_rating;
drop table if exists goal_snapshot;

alter table if exists customer
drop column if exists planning_style;
