package constants

const (
	GoalStateActive   = "active"
	GoalStateArchived = "archived"
	GoalStateDeleted  = "deleted"

	YMDLayout = "2006-01-02"

	InternalServerError = "Internal server error"

	NewRelicEndpoint = "https://log-api.newrelic.com/log/v1"
)
