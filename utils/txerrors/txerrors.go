package txerrors

import (
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

func Error(context string, err error) error {
	return TxError{
		Context: context,
		Err:     err,
	}
}

func Handle(tx *sqlx.Tx, err error) error {
	var txError TxError
	if errors.As(err, &txError) {
		rbErr := tx.Rollback()
		if rbErr != nil {
			err = fmt.Errorf("%s: rolling back database transaction: %s", err, rbErr)
		}
	}

	return err
}
