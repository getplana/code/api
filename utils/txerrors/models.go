package txerrors

import "fmt"

type TxError struct {
	Context string
	Err     error
}

func (e TxError) Error() string {
	return fmt.Sprintf("%s: %s", e.Context, e.Err)
}

func (e TxError) Unwrap() error {
	return e.Err
}
