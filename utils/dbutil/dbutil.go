package dbutil

import (
	"fmt"
	"os"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/utils/txerrors"
)

func ConnectDB(envVar string) (*sqlx.DB, error) {
	databaseURL := os.Getenv(envVar)
	if databaseURL == "" {
		err := fmt.Errorf("%s environment variable is not defined", envVar)
		return nil, err
	}

	db, err := sqlx.Connect("postgres", databaseURL)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func CommitTx(tx *sqlx.Tx) error {
	err := tx.Commit()
	if err != nil {
		err = txerrors.Error("committing database transaction", err)
		return err
	}

	return nil
}
