package update

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/jmoiron/sqlx"
)

// Updates with non-nil values of item update struct.
func Update(tx *sqlx.Tx, tableName string, itemID int, itemUpdate interface{}) error {
	structType := reflect.TypeOf(itemUpdate)
	structValue := reflect.ValueOf(itemUpdate)
	numStructFields := structType.NumField()
	atLeast1 := false
	params := map[string]interface{}{}
	var setClause strings.Builder

	for i := 0; i < numStructFields; i++ {
		// Assumes that types for struct fields are pointers.
		field := structType.Field(i)
		fieldValuePtr := structValue.Field(i)

		if !fieldValuePtr.IsNil() {
			// Uses the 'json' tag since the update values are coming from the client.
			tag := field.Tag.Get("json")

			var exp string
			if atLeast1 {
				exp = fmt.Sprintf(", %s = :%s", tag, tag)
			} else {
				exp = fmt.Sprintf("%s = :%s", tag, tag)
				atLeast1 = true
			}

			setClause.WriteString(exp)

			// Source: https://stackoverflow.com/questions/18091562/how-to-get-underlying-value-from-a-reflect-value-in-golang/18109639#18109639
			fieldValue := fieldValuePtr.Elem()
			fieldType := fieldValue.Kind()
			switch fieldType {
			case reflect.Bool:
				params[tag] = fieldValue.Interface().(bool)
			case reflect.Int:
				params[tag] = fieldValue.Interface().(int)
			case reflect.String:
				params[tag] = fieldValue.Interface().(string)
			default:
				err := fmt.Errorf("unsupported field type %s", fieldType)
				return err
			}
		}
	}

	sql := `UPDATE %s
			SET %s
			WHERE id = :id`
	sql = fmt.Sprintf(sql, tableName, setClause.String())
	params["id"] = itemID

	_, err := tx.NamedExec(sql, params)
	if err != nil {
		return err
	}

	return nil
}
