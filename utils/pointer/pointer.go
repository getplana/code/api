package pointer

func Int(x int) *int {
	return &x
}

func String(x string) *string {
	return &x
}
