package timeutil

import (
	"time"

	"gitlab.com/getplana/api/constants"
)

// Assumes '2006-01-02' layout.
func CreateYMDTime(value string) time.Time {
	t, _ := time.Parse(constants.YMDLayout, value)
	return t
}
