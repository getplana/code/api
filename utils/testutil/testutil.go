package testutil

import (
	"fmt"
	"log"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/txerrors"
)

func ConnectDB() *sqlx.DB {
	var err error
	defer func() {
		if err != nil {
			log.Fatalf("Error with connecting to database: %s", err)
		}
	}()

	// Working directory taken from Dockerfile.test.
	err = godotenv.Load("/usr/src/.env")
	if err != nil {
		err = fmt.Errorf("loading .env file: %s", err)
		return nil
	}

	db, err := dbutil.ConnectDB("TEST_DATABASE_URL")
	if err != nil {
		return nil
	}

	return db
}

func ExecInserts(db *sqlx.DB, inserts []*goqu.InsertDataset) error {
	tx := db.MustBegin()
	var err error
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	for i := 0; i < len(inserts); i++ {
		sql, _, err := inserts[i].ToSQL()
		if err != nil {
			err = txerrors.Error("generating SQL", err)
			return err
		}

		_, err = tx.Exec(sql)
		if err != nil {
			err = txerrors.Error("executing SQL", err)
			return err
		}
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return err
	}

	return nil
}

func ClearAndSeedDB(db *sqlx.DB, inserts []*goqu.InsertDataset) {
	var err error
	defer func() {
		if err != nil {
			log.Fatalf("Error with seeding database: %s", err)
		}
	}()

	// Concatenate table names.
	var tablesStr strings.Builder
	for i := 0; i < len(tables); i++ {
		tablesStr.WriteString(tables[i])
		if i < len(tables)-1 {
			tablesStr.WriteString(", ")
		}
	}

	// Clear database.
	sql := fmt.Sprintf("TRUNCATE %s RESTART IDENTITY CASCADE", tablesStr.String())
	_, err = db.Exec(sql)
	if err != nil {
		err = fmt.Errorf("clearing database: %s", err)
		return
	}

	// Execute inserts.
	err = ExecInserts(db, inserts)
	if err != nil {
		err = fmt.Errorf("executing inserts: %s", err)
		return
	}
}
