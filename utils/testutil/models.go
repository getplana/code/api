package testutil

import (
	"github.com/doug-martin/goqu/v9"
	"gitlab.com/getplana/api/services/colors"
)

type Customer struct {
	ID int `db:"id"`
}

var (
	InsertCustomer = goqu.Insert("customer").Rows(
		Customer{
			ID: 1,
		},
	)

	// Taken from database migration file
	Red = colors.HSL{
		Hue:        0,
		Saturation: 100,
		Lightness:  40,
	}
	Orange = colors.HSL{
		Hue:        29,
		Saturation: 87,
		Lightness:  45,
	}
	Yellow = colors.HSL{
		Hue:        46,
		Saturation: 99,
		Lightness:  44,
	}

	tables = []string{
		"customer",
		"goal",
		"task_group",
		"task",
	}
)
