module gitlab.com/getplana/api

go 1.16

require (
	github.com/doug-martin/goqu/v9 v9.15.1
	github.com/go-chi/chi/v5 v5.0.1
	github.com/jmoiron/sqlx v1.3.1
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.1
	github.com/rs/cors v1.7.0
	github.com/stretchr/testify v1.7.0
)
