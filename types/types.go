package types

import (
	"database/sql/driver"
	"fmt"
	"strings"
	"time"

	"gitlab.com/getplana/api/constants"
)

type DateTime time.Time

// Converts alias to date string (in format of '2006-01-02') for JSON.
func (d *DateTime) MarshalJSON() ([]byte, error) {
	timeStr := time.Time(*d).Format(constants.YMDLayout)
	quoted := "\"" + timeStr + "\""
	return []byte(quoted), nil
}

// Converts date string from JSON (in format of '2006-01-02') to alias.
func (d *DateTime) UnmarshalJSON(b []byte) error {
	s := strings.Trim(string(b), "\"")

	parsed, err := time.Parse(constants.YMDLayout, s)
	if err != nil {
		err = fmt.Errorf("could not parse %s with layout %s: %s", s, constants.YMDLayout, err)
		return err
	}

	*d = DateTime(parsed)

	return nil
}

// Converts alias to time.Time when binding variables in sql queries.
func (d DateTime) Value() (driver.Value, error) {
	return driver.Value(time.Time(d)), nil
}
