package main

import (
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/rs/cors"
	"gitlab.com/getplana/api/services/colors"
	"gitlab.com/getplana/api/services/goals"
	"gitlab.com/getplana/api/services/logs"
	"gitlab.com/getplana/api/services/taskgroups"
	"gitlab.com/getplana/api/services/tasks"
	"gitlab.com/getplana/api/utils/dbutil"
)

func main() {
	err := godotenv.Load()
  if err != nil {
    log.Fatal("Error loading .env file")
  }

	envVar := "PORT"
	port := os.Getenv(envVar)
	if port == "" {
		log.Fatalf("%s environment variable is not defined", envVar)
	}

	db, err := dbutil.ConnectDB("DATABASE_URL")
	if err != nil {
		log.Fatalf("Error with connecting to database: %s", err)
	}

	envVar = "UI_URL"
	uiURL := os.Getenv(envVar)
	if uiURL == "" {
		log.Fatalf("%s environment variable is not defined", envVar)
	}

	// Define HTTP response headers.
	c := cors.New(cors.Options{
		AllowedOrigins: []string{uiURL},
		AllowedMethods: []string{"GET", "POST", "PATCH", "DELETE"},
	})

	router := chi.NewRouter()
	router.Use(c.Handler)
	router.Mount("/colors", colors.Routes(db))
	router.Mount("/goals", goals.Routes(db))
	router.Mount("/tasks", tasks.Routes(db))
	router.Mount("/task-groups", taskgroups.Routes(db))
	router.Mount("/logs", logs.Routes(db))

	err = http.ListenAndServe(":"+port, router)
	if err != nil {
		log.Fatalf("Error with starting server: %s", err)
	}
}
