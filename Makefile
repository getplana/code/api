# 'heroku local' reads .env
# Source: https://devcenter.heroku.com/articles/heroku-local#set-up-your-local-environment-variables
start:
	go build -o bin/api ./cmd/api
	heroku local

build:
	go build -o bin/api ./cmd/api

# Run docker-compose down first to rerun database migrations to test them.
test:
	docker-compose down
	docker-compose up
