package tasks

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/txerrors"
	"gitlab.com/getplana/api/utils/update"
)

func Update(db *sqlx.DB, taskID int, taskUpdate TaskUpdate) error {
	var err error
	tx := db.MustBegin()
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	err = update.Update(
		tx,
		"task",
		taskID,
		taskUpdate)
	if err != nil {
		context := fmt.Sprintf("updating task %d", taskID)
		err = txerrors.Error(context, err)
		return err
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return err
	}

	return nil
}
