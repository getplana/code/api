package tests

import (
	"fmt"
	"testing"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	goaltest "gitlab.com/getplana/api/services/goals/tests"
	grouptest "gitlab.com/getplana/api/services/taskgroups/tests"
	"gitlab.com/getplana/api/services/tasks"
	"gitlab.com/getplana/api/utils/testutil"
	"gitlab.com/getplana/api/utils/timeutil"
)

func TestTaskReorder(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("move task to another group", func(t *testing.T) {
		seedReorder(db)

		taskID := 1
		newGroupID := 2
		newGroupOrder := 1
		err := tasks.Reorder(db, taskID, &newGroupID, newGroupOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		group1Tasks, err := getTasksForGroup(db, 1)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		group2Tasks, err := getTasksForGroup(db, 2)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 2, len(group1Tasks))
		assert.Equal(t, 3, len(group2Tasks))

		assert.Equal(t, 2, group1Tasks[0].TaskID)
		assert.Equal(t, 3, group1Tasks[1].TaskID)
		assert.Equal(t, 1, group2Tasks[0].TaskID)
		assert.Equal(t, 4, group2Tasks[1].TaskID)
		assert.Equal(t, 5, group2Tasks[2].TaskID)

		assert.Equal(t, 1, group1Tasks[0].GroupOrder)
		assert.Equal(t, 2, group1Tasks[1].GroupOrder)
		assert.Equal(t, 1, group2Tasks[0].GroupOrder)
		assert.Equal(t, 2, group2Tasks[1].GroupOrder)
		assert.Equal(t, 3, group2Tasks[2].GroupOrder)
	})

	t.Run("move task down in same group", func(t *testing.T) {
		seedReorder(db)

		taskID := 1
		newGroupOrder := 3
		err := tasks.Reorder(db, taskID, nil, newGroupOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		group1Tasks, err := getTasksForGroup(db, 1)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 1, group1Tasks[1].GroupOrder)
		assert.Equal(t, 2, group1Tasks[2].GroupOrder)
		assert.Equal(t, 3, group1Tasks[0].GroupOrder)
	})

	t.Run("move task up in same group", func(t *testing.T) {
		seedReorder(db)

		taskID := 3
		newGroupOrder := 1
		err := tasks.Reorder(db, taskID, nil, newGroupOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		group1Tasks, err := getTasksForGroup(db, 1)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 1, group1Tasks[2].GroupOrder)
		assert.Equal(t, 2, group1Tasks[0].GroupOrder)
		assert.Equal(t, 3, group1Tasks[1].GroupOrder)
	})

	t.Run("move task down one", func(t *testing.T) {
		seedReorder(db)

		taskID := 1
		newGroupOrder := 2
		err := tasks.Reorder(db, taskID, nil, newGroupOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		group1Tasks, err := getTasksForGroup(db, 1)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 1, group1Tasks[1].GroupOrder)
		assert.Equal(t, 2, group1Tasks[0].GroupOrder)
		assert.Equal(t, 3, group1Tasks[2].GroupOrder)
	})

	t.Run("move task up one", func(t *testing.T) {
		seedReorder(db)

		taskID := 3
		newGroupOrder := 2
		err := tasks.Reorder(db, taskID, nil, newGroupOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		group1Tasks, err := getTasksForGroup(db, 1)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 1, group1Tasks[0].GroupOrder)
		assert.Equal(t, 2, group1Tasks[2].GroupOrder)
		assert.Equal(t, 3, group1Tasks[1].GroupOrder)
	})
}

func seedReorder(db *sqlx.DB) {
	insertGroup1 := grouptest.InsertTaskGroup
	insertGroup2 := goqu.Insert("task_group").Rows(
		grouptest.TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "",
			DayOrder:   2,
		},
	)

	insertGroup1Tasks := goqu.Insert("task").Rows(
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    1,
			GroupOrder: 1,
			Text:       "",
			GoalID:     1,
		},
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    1,
			GroupOrder: 2,
			Text:       "",
			GoalID:     1,
		},
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    1,
			GroupOrder: 3,
			Text:       "",
			GoalID:     1,
		},
	)
	insertGroup2Tasks := goqu.Insert("task").Rows(
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    2,
			GroupOrder: 1,
			Text:       "",
			GoalID:     1,
		},
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    2,
			GroupOrder: 2,
			Text:       "",
			GoalID:     1,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		goaltest.InsertGoal,
		insertGroup1,
		insertGroup2,
		insertGroup1Tasks,
		insertGroup2Tasks,
	})
}

func getTasksForGroup(db *sqlx.DB, groupID int) ([]tasks.TaskOrder, error) {
	selectTaskOrders, _, err := SelectTaskOrders.
		Where(goqu.Ex{"group_id": groupID}).
		Order(goqu.C("id").Asc()).
		ToSQL()
	if err != nil {
		return nil, err
	}

	tasks := []tasks.TaskOrder{}
	err = db.Select(&tasks, selectTaskOrders)
	if err != nil {
		return nil, err
	}

	return tasks, nil
}
