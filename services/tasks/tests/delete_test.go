package tests

import (
	"fmt"
	"testing"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	goaltest "gitlab.com/getplana/api/services/goals/tests"
	grouptest "gitlab.com/getplana/api/services/taskgroups/tests"
	"gitlab.com/getplana/api/services/tasks"
	"gitlab.com/getplana/api/utils/testutil"
	"gitlab.com/getplana/api/utils/timeutil"
)

func TestTaskDelete(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("basic delete", func(t *testing.T) {
		seedBasicDelete(db)

		taskID := 1
		err := tasks.Delete(db, taskID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		tasks, err := getTasks(db)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 0, len(tasks))
	})

	t.Run("delete updates group orders in same group", func(t *testing.T) {
		seedGroupOrders(db)

		taskID := 1
		err := tasks.Delete(db, taskID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		group1ID := 1
		tasks, err := getTasksForGroup(db, group1ID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 2, len(tasks))

		assert.Equal(t, 2, tasks[0].TaskID)
		assert.Equal(t, 1, tasks[0].GroupOrder)

		assert.Equal(t, 3, tasks[1].TaskID)
		assert.Equal(t, 2, tasks[1].GroupOrder)
	})

	t.Run("delete does *not* update group orders in other groups", func(t *testing.T) {
		seedGroupOrders(db)

		taskID := 1
		err := tasks.Delete(db, taskID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		group1ID := 2
		tasks, err := getTasksForGroup(db, group1ID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 2, len(tasks))

		assert.Equal(t, 4, tasks[0].TaskID)
		assert.Equal(t, 5, tasks[1].TaskID)

		assert.Equal(t, 1, tasks[0].GroupOrder)
		assert.Equal(t, 2, tasks[1].GroupOrder)
	})
}

func seedBasicDelete(db *sqlx.DB) {
	insertTask := goqu.Insert("task").Rows(
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    1,
			GroupOrder: 1,
			Text:       "",
			GoalID:     1,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		goaltest.InsertGoal,
		grouptest.InsertTaskGroup,
		insertTask,
	})
}

func seedGroupOrders(db *sqlx.DB) {
	insertGroup1 := grouptest.InsertTaskGroup
	insertGroup2 := goqu.Insert("task_group").Rows(
		grouptest.TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "",
			DayOrder:   2,
		},
	)

	insertGroup1Tasks := goqu.Insert("task").Rows(
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    1,
			GroupOrder: 1,
			Text:       "",
			GoalID:     1,
		},
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    1,
			GroupOrder: 2,
			Text:       "",
			GoalID:     1,
		},
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    1,
			GroupOrder: 3,
			Text:       "",
			GoalID:     1,
		},
	)
	insertGroup2Tasks := goqu.Insert("task").Rows(
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    2,
			GroupOrder: 1,
			Text:       "",
			GoalID:     1,
		},
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    2,
			GroupOrder: 2,
			Text:       "",
			GoalID:     1,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		goaltest.InsertGoal,
		insertGroup1,
		insertGroup2,
		insertGroup1Tasks,
		insertGroup2Tasks,
	})
}

func getTasks(db *sqlx.DB) ([]tasks.TaskOrder, error) {
	selectTaskOrders, _, err := SelectTaskOrders.ToSQL()
	if err != nil {
		return nil, err
	}

	tasks := []tasks.TaskOrder{}
	err = db.Select(&tasks, selectTaskOrders)
	if err != nil {
		return nil, err
	}

	return tasks, nil
}
