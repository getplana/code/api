package tests

import (
	"fmt"
	"testing"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/getplana/api/constants"
	goaltest "gitlab.com/getplana/api/services/goals/tests"
	grouptest "gitlab.com/getplana/api/services/taskgroups/tests"
	"gitlab.com/getplana/api/services/tasks"
	"gitlab.com/getplana/api/utils/testutil"
	"gitlab.com/getplana/api/utils/timeutil"
)

func TestTaskGet(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("get from date to 5 days before", func(t *testing.T) {
		seedGetFromDate(db)

		numDaysBefore := 5
		days, err := tasks.Get(db, customerID, jan3, numDaysBefore)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test number of days.
		assert.Equal(t, numDaysBefore, len(days))

		// Test dates.
		assert.Equal(t, jan3Str, time.Time(days[0].Date).Format(constants.YMDLayout))
		assert.Equal(t, jan2Str, time.Time(days[1].Date).Format(constants.YMDLayout))
		assert.Equal(t, jan1Str, time.Time(days[2].Date).Format(constants.YMDLayout))
		assert.Equal(t, "2021-12-31", time.Time(days[3].Date).Format(constants.YMDLayout))
		assert.Equal(t, "2021-12-30", time.Time(days[4].Date).Format(constants.YMDLayout))

		// Test number of groups for each day.
		assert.Equal(t, 3, len(days[0].TaskGroups))
		assert.Equal(t, 1, len(days[1].TaskGroups))
		assert.Equal(t, 1, len(days[2].TaskGroups))
		assert.Equal(t, 0, len(days[3].TaskGroups))
		assert.Equal(t, 0, len(days[4].TaskGroups))

		// Test groups.
		assert.Equal(t, jan3Group1ID, days[0].TaskGroups[0].ID)
		assert.Equal(t, jan3Group1Name, days[0].TaskGroups[0].Name)
		assert.Equal(t, jan3Group1Order, days[0].TaskGroups[0].DayOrder)
		assert.Equal(t, jan3Group2ID, days[0].TaskGroups[1].ID)
		assert.Equal(t, jan3Group2Name, days[0].TaskGroups[1].Name)
		assert.Equal(t, jan3Group2Order, days[0].TaskGroups[1].DayOrder)
		assert.Equal(t, jan3Group3ID, days[0].TaskGroups[2].ID)
		assert.Equal(t, jan3Group3Name, days[0].TaskGroups[2].Name)
		assert.Equal(t, jan3Group3Order, days[0].TaskGroups[2].DayOrder)
		assert.Equal(t, jan2Group1ID, days[1].TaskGroups[0].ID)
		assert.Equal(t, jan2Group1Name, days[1].TaskGroups[0].Name)
		assert.Equal(t, jan2Group1Order, days[1].TaskGroups[0].DayOrder)
		assert.Equal(t, jan1Group1ID, days[2].TaskGroups[0].ID)
		assert.Equal(t, jan1Group1Name, days[2].TaskGroups[0].Name)
		assert.Equal(t, jan1Group1Order, days[2].TaskGroups[0].DayOrder)

		// Test number of tasks for each group.
		assert.Equal(t, 3, len(days[0].TaskGroups[0].Tasks))
		assert.Equal(t, 1, len(days[0].TaskGroups[1].Tasks))
		assert.Equal(t, 1, len(days[0].TaskGroups[2].Tasks))
		assert.Equal(t, 1, len(days[1].TaskGroups[0].Tasks))
		assert.Equal(t, 1, len(days[2].TaskGroups[0].Tasks))

		// Test tasks.
		assert.True(t, Equal(days[0].TaskGroups[0].Tasks[0], jan3Group1Task1, goal1Text, testutil.Red))
		assert.True(t, Equal(days[0].TaskGroups[0].Tasks[1], jan3Group1Task2, goal2Text, testutil.Orange))
		assert.True(t, Equal(days[0].TaskGroups[0].Tasks[2], jan3Group1Task3, goal3Text, testutil.Yellow))
		assert.True(t, Equal(days[0].TaskGroups[1].Tasks[0], jan3Group2Task1, goal1Text, testutil.Red))
		assert.True(t, Equal(days[0].TaskGroups[2].Tasks[0], jan3Group3Task1, goal1Text, testutil.Red))
		assert.True(t, Equal(days[1].TaskGroups[0].Tasks[0], jan2Group1Task1, goal1Text, testutil.Red))
		assert.True(t, Equal(days[2].TaskGroups[0].Tasks[0], jan1Group1Task1, goal1Text, testutil.Red))
	})
}

var (
	customerID = 1
	jan3Str    = "2022-01-03"
	jan2Str    = "2022-01-02"
	jan1Str    = "2022-01-01"
	jan3       = timeutil.CreateYMDTime(jan3Str)
	jan2       = timeutil.CreateYMDTime(jan2Str)
	jan1       = timeutil.CreateYMDTime(jan1Str)

	// Goals
	goal1Text = "Goal #1"
	goal2Text = "Goal #2"
	goal3Text = "Goal #3"
	goal1     = goaltest.Goal{
		CustomerID:  customerID,
		State:       constants.GoalStateActive,
		Text:        goal1Text,
		ColorID:     1,
		OrderNumber: 1,
	}
	goal2 = goaltest.Goal{
		CustomerID:  customerID,
		State:       constants.GoalStateActive,
		Text:        goal2Text,
		ColorID:     2,
		OrderNumber: 2,
	}
	goal3 = goaltest.Goal{
		CustomerID:  customerID,
		State:       constants.GoalStateActive,
		Text:        goal3Text,
		ColorID:     3,
		OrderNumber: 3,
	}

	// Groups
	jan3Group1ID    = 1
	jan3Group2ID    = 2
	jan3Group3ID    = 3
	jan2Group1ID    = 4
	jan1Group1ID    = 5
	jan3Group1Order = 1
	jan3Group2Order = 2
	jan3Group3Order = 3
	jan2Group1Order = 1
	jan1Group1Order = 1
	jan3Group1Name  = "Jan 3 Group #1"
	jan3Group2Name  = "Jan 3 Group #2"
	jan3Group3Name  = "Jan 3 Group #3"
	jan2Group1Name  = "Jan 2 Group #1"
	jan1Group1Name  = "Jan 1 Group #1"
	jan3Group1      = grouptest.TaskGroup{
		CustomerID: customerID,
		Date:       jan3,
		Name:       jan3Group1Name,
		DayOrder:   jan3Group1Order,
	}
	jan3Group2 = grouptest.TaskGroup{
		CustomerID: customerID,
		Date:       jan3,
		Name:       jan3Group2Name,
		DayOrder:   jan3Group2Order,
	}
	jan3Group3 = grouptest.TaskGroup{
		CustomerID: customerID,
		Date:       jan3,
		Name:       jan3Group3Name,
		DayOrder:   jan3Group3Order,
	}
	jan2Group1 = grouptest.TaskGroup{
		CustomerID: customerID,
		Date:       jan2,
		Name:       jan2Group1Name,
		DayOrder:   jan2Group1Order,
	}
	jan1Group1 = grouptest.TaskGroup{
		CustomerID: customerID,
		Date:       jan1,
		Name:       jan1Group1Name,
		DayOrder:   jan1Group1Order,
	}

	// Tasks
	jan3Group1Task1 = Task{
		CustomerID:  customerID,
		Date:        jan3,
		GroupID:     jan3Group1ID,
		Text:        "Jan 3 Task #1",
		GoalID:      1,
		IsCompleted: true,
		GroupOrder:  1,
	}
	jan3Group1Task2 = Task{
		CustomerID:  customerID,
		Date:        jan3,
		GroupID:     jan3Group1ID,
		Text:        "Jan 3 Task #2",
		GoalID:      2,
		IsCompleted: true,
		GroupOrder:  2,
	}
	jan3Group1Task3 = Task{
		CustomerID:  customerID,
		Date:        jan3,
		GroupID:     jan3Group1ID,
		Text:        "Jan 3 Task #3",
		GoalID:      3,
		IsCompleted: false,
		GroupOrder:  3,
	}
	jan3Group2Task1 = Task{
		CustomerID:  customerID,
		Date:        jan2,
		GroupID:     jan3Group2ID,
		Text:        "Jan 2 Task #1",
		GoalID:      1,
		IsCompleted: true,
		GroupOrder:  1,
	}
	jan3Group3Task1 = Task{
		CustomerID:  customerID,
		Date:        jan1,
		GroupID:     jan3Group3ID,
		Text:        "Jan 1 Task #1",
		GoalID:      1,
		IsCompleted: true,
		GroupOrder:  1,
	}
	jan2Group1Task1 = Task{
		CustomerID:  customerID,
		Date:        jan2,
		GroupID:     jan2Group1ID,
		Text:        "Jan 2 Task #1",
		GoalID:      1,
		IsCompleted: true,
		GroupOrder:  1,
	}
	jan1Group1Task1 = Task{
		CustomerID:  customerID,
		Date:        jan1,
		GroupID:     jan1Group1ID,
		Text:        "Jan 1 Task #1",
		GoalID:      1,
		IsCompleted: true,
		GroupOrder:  1,
	}
)

func seedGetFromDate(db *sqlx.DB) {
	insertGoals := goqu.Insert("goal").Rows(
		goal1,
		goal2,
		goal3,
	)

	insertGroups := goqu.Insert("task_group").Rows(
		jan3Group1,
		jan3Group2,
		jan3Group3,
		jan2Group1,
		jan1Group1,
	)

	insertTasks := goqu.Insert("task").Rows(
		jan3Group1Task1,
		jan3Group1Task2,
		jan3Group1Task3,
		jan3Group2Task1,
		jan3Group3Task1,
		jan2Group1Task1,
		jan1Group1Task1,
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		insertGoals,
		insertGroups,
		insertTasks,
	})
}
