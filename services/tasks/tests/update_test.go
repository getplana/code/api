package tests

import (
	"fmt"
	"testing"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/getplana/api/constants"
	goaltest "gitlab.com/getplana/api/services/goals/tests"
	grouptest "gitlab.com/getplana/api/services/taskgroups/tests"
	"gitlab.com/getplana/api/services/tasks"
	"gitlab.com/getplana/api/utils/testutil"
	"gitlab.com/getplana/api/utils/timeutil"
)

func TestTaskUpdate(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("basic update", func(t *testing.T) {
		seedBasicUpdate(db)

		// Initialize task update.
		newText := "new text"
		newGoal := 2
		newIsCompleted := true
		taskUpdate := tasks.TaskUpdate{
			Text:        &newText,
			GoalID:      &newGoal,
			IsCompleted: &newIsCompleted,
		}

		// Update task.
		err := tasks.Update(db, 1, taskUpdate)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Generate SQL.
		selectTask, _, err := SelectTasks.ToSQL()
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Get updated task.
		updatedTask := Task{}
		err = db.Get(&updatedTask, selectTask)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test updated task.
		assert.Equal(t, newText, updatedTask.Text)
		assert.Equal(t, newGoal, updatedTask.GoalID)
		assert.Equal(t, newIsCompleted, updatedTask.IsCompleted)
	})
}

func seedBasicUpdate(db *sqlx.DB) {
	insert2ndGoal := goqu.Insert("goal").Rows(
		goaltest.Goal{
			CustomerID:  1,
			Text:        "",
			State:       constants.GoalStateActive,
			ColorID:     1,
			OrderNumber: 2,
		},
	)

	insert2ndTaskGroup := goqu.Insert("task_group").Rows(
		grouptest.TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "",
			DayOrder:   2,
		},
	)

	insertTask := goqu.Insert("task").Rows(
		Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    1,
			GroupOrder: 1,
			Text:       "",
			GoalID:     1,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		goaltest.InsertGoal,
		insert2ndGoal,
		grouptest.InsertTaskGroup,
		insert2ndTaskGroup,
		insertTask,
	})
}
