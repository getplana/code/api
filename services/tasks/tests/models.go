package tests

import (
	"time"

	"github.com/doug-martin/goqu/v9"
	"gitlab.com/getplana/api/services/colors"
	"gitlab.com/getplana/api/services/tasks"
)

type Task struct {
	CustomerID  int       `db:"customer_id"`
	Date        time.Time `db:"date"`
	GroupID     int       `db:"group_id"`
	GroupOrder  int       `db:"group_order"`
	Text        string    `db:"text"`
	GoalID      int       `db:"goal_id"`
	IsCompleted bool      `db:"is_completed"`
}

func Equal(task1 tasks.Task, task2 Task, goalText string, hsl colors.HSL) bool {
	return task1.Text == task2.Text &&
		task1.GoalID == task2.GoalID &&
		task1.GoalText == goalText &&
		task1.IsCompleted == task2.IsCompleted &&
		task1.GroupOrder == task2.GroupOrder &&
		task1.Hue == hsl.Hue &&
		task1.Saturation == hsl.Saturation &&
		task1.Lightness == hsl.Lightness
}

var (
	SelectTasks      = goqu.Select(&Task{}).From("task")
	SelectTaskOrders = goqu.Select(&tasks.TaskOrder{}).From("task")
)
