package tests

import (
	"fmt"
	"testing"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	goaltest "gitlab.com/getplana/api/services/goals/tests"
	grouptest "gitlab.com/getplana/api/services/taskgroups/tests"
	"gitlab.com/getplana/api/services/tasks"
	"gitlab.com/getplana/api/types"
	"gitlab.com/getplana/api/utils/testutil"
	"gitlab.com/getplana/api/utils/timeutil"
)

func TestTaskCreate(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("basic create", func(t *testing.T) {
		seedBasicCreate(db)

		customerID := 1
		date := types.DateTime(timeutil.CreateYMDTime("2021-01-31"))
		groupID := 1
		groupOrder := 1
		text := "text"
		goalID := 1
		isCompleted := true
		newTask := tasks.NewTask{
			CustomerID:  &customerID,
			Date:        &date,
			GroupID:     &groupID,
			GroupOrder:  &groupOrder,
			Text:        &text,
			GoalID:      &goalID,
			IsCompleted: &isCompleted,
		}

		_, err := tasks.Create(db, newTask)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		taskID := 1
		createdTask, err := getTask(db, taskID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, customerID, createdTask.CustomerID)
		assert.True(t, createdTask.Date.Equal(time.Time(date)))
		assert.Equal(t, groupID, createdTask.GroupID)
		assert.Equal(t, groupOrder, createdTask.GroupOrder)
		assert.Equal(t, text, createdTask.Text)
		assert.Equal(t, goalID, createdTask.GoalID)
		assert.Equal(t, isCompleted, createdTask.IsCompleted)
	})
}

func seedBasicCreate(db *sqlx.DB) {
	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		goaltest.InsertGoal,
		grouptest.InsertTaskGroup,
	})
}

func getTask(db *sqlx.DB, taskID int) (*Task, error) {
	selectTask, _, err := SelectTasks.
		Where(goqu.Ex{"id": taskID}).
		ToSQL()
	if err != nil {
		return nil, err
	}

	task := Task{}
	err = db.Get(&task, selectTask)
	if err != nil {
		return nil, err
	}

	return &task, nil
}
