package tasks

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/txerrors"
	"gitlab.com/getplana/api/utils/update"
)

func Reorder(db *sqlx.DB, taskID int, newGroupID *int, newGroupOrder int) error {
	taskToReorder, err := getTaskToReorder(db, taskID)
	if err != nil {
		err = fmt.Errorf("getting task to reorder: %s", err)
		return err
	}

	tx := db.MustBegin()
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	if newGroupID == nil {
		oldGroupOrder := taskToReorder.GroupOrder
		err := reorderInSameGroup(tx, *taskToReorder, oldGroupOrder, newGroupOrder)
		if err != nil {
			return err
		}
	} else {
		err = reorderToDifferentGroup(tx, *taskToReorder, *newGroupID, newGroupOrder)
		if err != nil {
			return err
		}
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return err
	}

	return nil
}

func getTaskToReorder(db *sqlx.DB, taskID int) (*TaskToReorder, error) {
	sql := `SELECT id,
				   customer_id,
				   date,
				   group_id,
				   group_order
			FROM task
			WHERE id = $1`

	taskToReorder := TaskToReorder{}
	err := db.Get(
		&taskToReorder,
		sql,
		taskID) // $1
	if err != nil {
		return nil, err
	}

	return &taskToReorder, nil
}

func reorderInSameGroup(
	tx *sqlx.Tx,
	taskToReorder TaskToReorder,
	oldGroupOrder int,
	newGroupOrder int,
) error {
	taskID := taskToReorder.ID
	customerID := taskToReorder.CustomerID
	date := taskToReorder.Date
	groupID := taskToReorder.GroupID

	var topGroupOrder int
	var botGroupOrder int
	var topInclusive bool
	var botInclusive bool
	var shiftChange int
	var shiftDirection string
	var tasksToShiftPosition string
	if oldGroupOrder < newGroupOrder {
		topGroupOrder = oldGroupOrder
		botGroupOrder = newGroupOrder
		topInclusive = false
		botInclusive = true
		shiftChange = -1
		shiftDirection = "up"
		tasksToShiftPosition = "above"
	} else if oldGroupOrder > newGroupOrder {
		topGroupOrder = newGroupOrder
		botGroupOrder = oldGroupOrder
		topInclusive = true
		botInclusive = false
		shiftChange = 1
		shiftDirection = "down"
		tasksToShiftPosition = "below"
	}

	err := shiftTasks(tx, customerID, date, groupID, topGroupOrder, topInclusive, &botGroupOrder, &botInclusive, shiftChange)
	if err != nil {
		context := fmt.Sprintf("shifting %s tasks %s moved task", shiftDirection, tasksToShiftPosition)
		err = txerrors.Error(context, err)
		return err
	}

	// Update group order after shift tasks to avoid also shifting task to be moved.
	taskUpdate := TaskUpdate{GroupOrder: &newGroupOrder}
	err = update.Update(tx, "task", taskID, taskUpdate)
	if err != nil {
		context := fmt.Sprintf("moving task %d from group order %d to %d in same group", taskID, oldGroupOrder, newGroupOrder)
		err = txerrors.Error(context, err)
		return err
	}

	return nil
}

func reorderToDifferentGroup(
	tx *sqlx.Tx,
	taskToReorder TaskToReorder,
	newGroupID int,
	newGroupOrder int,
) error {
	customerID := taskToReorder.CustomerID
	date := taskToReorder.Date
	oldGroupID := taskToReorder.GroupID
	oldGroupOrder := taskToReorder.GroupOrder

	// Don't include old group order since it's the task being moved.
	err := shiftTasks(tx, customerID, date, oldGroupID, oldGroupOrder, false, nil, nil, -1)
	if err != nil {
		err = txerrors.Error("shifting up tasks in old group below moved task", err)
		return err
	}

	// Include the new group order since it's currently occupying the position that the task being
	// moved wants to take.
	err = shiftTasks(tx, customerID, date, newGroupID, newGroupOrder, true, nil, nil, 1)
	if err != nil {
		err = txerrors.Error("shifting down tasks in new group below moved task", err)
		return err
	}

	// Update group order after shift tasks to avoid also shifting task to be moved.
	taskUpdate := TaskUpdate{GroupID: &newGroupID, GroupOrder: &newGroupOrder}
	err = update.Update(tx, "task", taskToReorder.ID, taskUpdate)
	if err != nil {
		context := fmt.Sprintf("moving task %d to group %d and group order %d", taskToReorder.ID, newGroupID, newGroupOrder)
		err = txerrors.Error(context, err)
		return err
	}

	return nil
}

func shiftTasks(
	tx *sqlx.Tx,
	customerID int,
	date time.Time,
	groupID int,
	topGroupOrder int,
	topInclusive bool,
	botGroupOrder *int,
	botInclusive *bool,
	shiftChange int,
) error {
	tasks, err := getTasksToShift(tx, customerID, date, groupID, topGroupOrder, topInclusive, botGroupOrder, botInclusive)
	if err != nil {
		err = fmt.Errorf("getting tasks to shift: %s", err)
		return err
	}

	for _, task := range tasks {
		newGroupOrder := task.GroupOrder + shiftChange
		taskUpdate := TaskUpdate{GroupOrder: &newGroupOrder}
		err := update.Update(tx, "task", task.TaskID, taskUpdate)
		if err != nil {
			err := fmt.Errorf("moving task %d from group order %d to %d in same group", task.TaskID, task.GroupOrder, newGroupOrder)
			return err
		}
	}

	return nil
}

func getTasksToShift(
	tx *sqlx.Tx,
	customerID int,
	date time.Time,
	groupID int,
	topGroupOrder int,
	topInclusive bool,
	botGroupOrder *int,
	botInclusive *bool,
) ([]TaskOrder, error) {
	sql := `SELECT id,
			       group_order
			FROM task
			WHERE customer_id = $1
			AND date = $2
			AND group_id = $3
			AND $4 %s group_order
			%s
			ORDER BY group_order`

	topOperator := "<"
	if topInclusive {
		topOperator = "<="
	}

	params := []interface{}{
		customerID,    // $1
		date,          // $2
		groupID,       // $3
		topGroupOrder, // $4
	}

	botClause := ""
	if botGroupOrder != nil && botInclusive != nil {
		if *botInclusive {
			botClause = "AND group_order <= $5"
		} else {
			botClause = "AND group_order < $5"
		}

		params = append(params, botGroupOrder) // $5
	}

	sql = fmt.Sprintf(sql, topOperator, botClause)
	tasks := []TaskOrder{}
	err := tx.Select(
		&tasks,
		sql,
		params...)
	if err != nil {
		return nil, err
	}

	return tasks, nil
}
