package tasks

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/txerrors"
)

func Create(db *sqlx.DB, newTask NewTask) (*Task, error) {
	tx := db.MustBegin()
	var err error
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	createdTask, err := createTask(tx, newTask)
	if err != nil {
		err = txerrors.Error("creating task", err)
		return nil, err
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return nil, err
	}

	return createdTask, nil
}

func createTask(tx *sqlx.Tx, newTask NewTask) (*Task, error) {
	isCompleted := false
	if newTask.IsCompleted != nil {
		isCompleted = *newTask.IsCompleted
	}

	sql := `
	WITH inserted_task AS (
		INSERT INTO task (
			customer_id,
			date,
			group_id,
			group_order,
			text,
			goal_id,
			is_completed
		) VALUES (
			$1,
			$2,
			$3,
			$4,
			$5,
			$6,
			$7
		) RETURNING *
	)
	SELECT
		it.id,
		it.text,
		g.id   AS goal_id,
		g.text AS goal_text,
		it.is_completed,
		it.group_order,
		c.hue,
		c.saturation,
		c.lightness
	FROM inserted_task AS it
	INNER JOIN goal    AS g ON it.goal_id = g.id
	INNER JOIN color   AS c ON g.color_id = c.id`

	createdTask := Task{}
	err := tx.Get(
		&createdTask,
		sql,
		newTask.CustomerID, // $1
		newTask.Date,       // $2
		newTask.GroupID,    // $3
		newTask.GroupOrder, // $4
		newTask.Text,       // $5
		newTask.GoalID,     // $6
		isCompleted)        // $7
	if err != nil {
		return nil, err
	}

	return &createdTask, nil
}
