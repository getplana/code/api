package tasks

import (
	"time"

	"github.com/jmoiron/sqlx"
)

func Get(db *sqlx.DB, customerID int, fromDate time.Time, numDaysBefore int) ([]Day, error) {
	// Plus one since number of days includes the start date.
	laterDate := fromDate
	earlierDate := laterDate.AddDate(0, 0, -numDaysBefore+1)

	sql := `
	WITH task_groups AS (
		SELECT tg.date,
			   tg.day_order,
			   tg.id,
			   tg.name,
			   json_agg(
				   json_build_object(
					   'id',           t.id,
					   'text',         t.text,
					   'goal_id',      g.id,
					   'goal_text',    g.text,
					   'is_completed', t.is_completed,
					   'group_id',     t.group_id,
					   'group_order',  t.group_order,
					   'color',        json_build_object(
						   'hue',        c.hue,
						   'saturation', c.saturation,
						   'lightness',  c.lightness	
					   )
				   ) ORDER BY group_order
			   ) AS tasks
		FROM task AS t
		INNER JOIN task_group AS tg ON t.group_id = tg.id
		INNER JOIN goal       AS g  ON t.goal_id = g.id
		INNER JOIN color      AS c  ON g.color_id = c.id
		WHERE t.customer_id = $1
		AND $2 <= t.date
		AND t.date <= $3
		GROUP BY t.group_id,
				 tg.id
	),
	series AS (
		SELECT date_trunc('day', timestamp)::date AS date
		FROM generate_series(
			$2,
			$3,
			'1 day'::interval
		) AS timestamp
	)
	SELECT series.date,
		   CASE
		       WHEN task_groups.date IS NULL THEN
			       '[]'::json
			   ELSE
			       json_agg(
				       json_build_object(
					       'day_order',  day_order,
						   'id',   id,
						   'name', name,
						   'tasks',      tasks
					   ) ORDER BY day_order
				   )
		   END AS task_groups
	FROM task_groups
	RIGHT OUTER JOIN series ON task_groups.date = series.date
	GROUP BY series.date,
			 task_groups.date
	ORDER BY series.date DESC`

	days := []Day{}
	err := db.Select(
		&days,
		sql,
		customerID,  // $1
		earlierDate, // $2
		laterDate)   // $3
	if err != nil {
		return nil, err
	}

	return days, nil
}
