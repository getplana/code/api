package tasks

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/getplana/api/services/colors"
	"gitlab.com/getplana/api/types"
)

// --- Types --- //

type Day struct {
	Date       types.DateTime `db:"date"   json:"date"`
	TaskGroups TaskGroups     `db:"task_groups" json:"task_groups"`
}

type TaskGroup struct {
	ID       int    `db:"id"        json:"id"`
	Name     string `db:"name"      json:"name"`
	DayOrder int    `db:"day_order" json:"day_order"`
	Tasks    Tasks  `db:"tasks"     json:"tasks"`
}

type Task struct {
	ID          int    `db:"id"           json:"id"`
	Text        string `db:"text"         json:"text"`
	GoalID      int    `db:"goal_id"      json:"goal_id"`
	GoalText    string `db:"goal_text"    json:"goal_text"`
	IsCompleted bool   `db:"is_completed" json:"is_completed"`
	GroupID     int    `db:"group_id" json:"group_id"`
	GroupOrder  int    `db:"group_order"  json:"group_order"`
	colors.HSL  `                         json:"color"`
}

type NewTask struct {
	CustomerID  *int            `json:"customer_id"`
	Date        *types.DateTime `json:"date"`
	GroupID     *int            `json:"group_id"`
	GroupOrder  *int            `json:"group_order"`
	Text        *string         `json:"text"`
	GoalID      *int            `json:"goal_id"`
	IsCompleted *bool           `json:"is_completed"`
}

type TaskUpdate struct {
	Text        *string `json:"text"`
	GoalID      *int    `json:"goal_id"`
	IsCompleted *bool   `json:"is_completed"`
	GroupID     *int    `json:"group_id"`
	GroupOrder  *int    `json:"group_order"`
}

type TaskToReorder struct {
	ID         int       `db:"id"`
	CustomerID int       `db:"customer_id"`
	Date       time.Time `db:"date"`
	GroupID    int       `db:"group_id"`
	GroupOrder int       `db:"group_order"`
}

type TaskToDelete struct {
	CustomerID int       `db:"customer_id"`
	Date       time.Time `db:"date"`
	GroupID    int       `db:"group_id"`
	GroupOrder int       `db:"group_order"`
}

type TaskOrder struct {
	TaskID     int `db:"id"`
	GroupOrder int `db:"group_order"`
}

/**
 * Converts data returned from the database into struct.
 *
 * Needed since sqlx doesn't automatically unmarshal JSON columns:
 * https://github.com/jmoiron/sqlx/issues/684.
 */

type TaskGroups []TaskGroup

func (tg *TaskGroups) Scan(src interface{}) error {
	switch value := src.(type) {
	case []byte:
		return json.Unmarshal(value, tg)
	case string:
		return json.Unmarshal([]byte(value), tg)
	default:
		return fmt.Errorf("unsupported type: %T", src)
	}
}

type Tasks []Task

func (t *Tasks) Scan(src interface{}) error {
	switch value := src.(type) {
	case []byte:
		return json.Unmarshal(value, t)
	case string:
		return json.Unmarshal([]byte(value), t)
	default:
		return fmt.Errorf("unsupported type: %T", src)
	}
}
