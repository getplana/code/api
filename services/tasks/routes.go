package tasks

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/constants"
)

func Routes(db *sqlx.DB) *chi.Mux {
	router := chi.NewRouter()
	router.Get("/", HandleGet(db))
	router.Post("/", HandlePost(db))
	router.Patch("/{task_id}", HandlePatch(db))
	router.Delete("/{task_id}", HandleDelete(db))

	return router
}

func HandleGet(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		customerIDStr := r.URL.Query().Get("customer_id")
		customerID, err := strconv.Atoi(customerIDStr)
		if err != nil {
			log.Printf("Invalid customer_id '%s': %s", customerIDStr, err)
			http.Error(w, "Invalid customer_id.", http.StatusBadRequest)
			return
		}

		fromDateStr := r.URL.Query().Get("from_date")
		fromDate, err := time.Parse(constants.YMDLayout, fromDateStr)
		if err != nil {
			log.Printf("Error with parsing %s with layout %s: %s", fromDateStr, constants.YMDLayout, err)
			http.Error(w, "From date must be in YYYY-MM-DD format.", http.StatusBadRequest)
			return
		}

		numDaysBeforeStr := r.URL.Query().Get("num_days_before")
		numDaysBefore, err := strconv.Atoi(numDaysBeforeStr)
		if err != nil {
			log.Printf("Invalid num_days_before '%s': %s", numDaysBeforeStr, err)
			http.Error(w, "Invalid num_days_before.", http.StatusBadRequest)
			return
		}

		tasks, err := Get(db, customerID, fromDate, numDaysBefore)
		if err != nil {
			log.Printf("Error with getting tasks: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(tasks)
		if err != nil {
			log.Printf("Error with encoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}
	}
}

func HandlePost(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		newTask := NewTask{}
		err := json.NewDecoder(r.Body).Decode(&newTask)
		if err != nil {
			log.Printf("Error with decoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		createdTask, err := Create(db, newTask)
		if err != nil {
			log.Printf("Error with creating task: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(createdTask)
		if err != nil {
			log.Printf("Error with encoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}
	}
}

func HandlePatch(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		taskIDStr := chi.URLParam(r, "task_id")
		taskID, err := strconv.Atoi(taskIDStr)
		msg := fmt.Sprintf("Invalid task ID '%s'", taskIDStr)
		if err != nil {
			log.Printf("%s: %s", msg, err)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}

		// Negative number is used as temporary ID for optimistic updates on UI.
		if taskID < 0 {
			log.Print(msg)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}

		taskUpdate := TaskUpdate{}
		err = json.NewDecoder(r.Body).Decode(&taskUpdate)
		if err != nil {
			log.Printf("Error with decoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		if taskUpdate.GroupOrder != nil {
			err = Reorder(db, taskID, taskUpdate.GroupID, *taskUpdate.GroupOrder)
			if err != nil {
				log.Printf("Error with reordering task: %s", err)
				http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
				return
			}
		} else {
			err = Update(db, taskID, taskUpdate)
			if err != nil {
				log.Printf("Error with updating task: %s", err)
				http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
				return
			}
		}
	}
}

func HandleDelete(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		taskIDStr := chi.URLParam(r, "task_id")
		taskID, err := strconv.Atoi(taskIDStr)
		msg := fmt.Sprintf("Invalid task ID '%s'", taskIDStr)
		if err != nil {
			log.Printf("%s: %s", msg, err)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}

		// Negative number is used as temporary ID for optimistic updates on UI.
		if taskID < 0 {
			log.Print(msg)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}

		err = Delete(db, taskID)
		if err != nil {
			log.Printf("Error with deleting task: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}
	}
}
