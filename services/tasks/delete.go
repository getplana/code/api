package tasks

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/txerrors"
	"gitlab.com/getplana/api/utils/update"
)

func Delete(db *sqlx.DB, taskID int) error {
	tx := db.MustBegin()
	var err error
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	err = shiftUpTasksAfterTaskToDelete(tx, taskID)
	if err != nil {
		err = txerrors.Error("shifting up tasks after task to delete", err)
		return err
	}

	err = deleteTask(tx, taskID)
	if err != nil {
		err = txerrors.Error("deleting task", err)
		return err
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return err
	}

	return nil
}

func shiftUpTasksAfterTaskToDelete(tx *sqlx.Tx, taskID int) error {
	taskToDelete, err := getTaskToDelete(tx, taskID)
	if err != nil {
		err = fmt.Errorf("getting task to delete: %s", err)
		return err
	}

	tasks, err := getTasksAfterTaskToDelete(tx, *taskToDelete)
	if err != nil {
		err = fmt.Errorf("getting tasks after task to delete: %s", err)
		return err
	}

	for _, task := range tasks {
		newGroupOrder := task.GroupOrder - 1
		taskUpdate := TaskUpdate{GroupOrder: &newGroupOrder}
		err := update.Update(tx, "task", task.TaskID, taskUpdate)
		if err != nil {
			err := fmt.Errorf("moving task %d from group order %d to %d", task.TaskID, task.GroupOrder, newGroupOrder)
			return err
		}
	}

	return nil
}

func getTaskToDelete(tx *sqlx.Tx, taskID int) (*TaskToDelete, error) {
	sql := `SELECT customer_id,
				   date,
				   group_id,
				   group_order
			FROM task
			WHERE id = $1`

	taskToDelete := TaskToDelete{}
	err := tx.Get(
		&taskToDelete,
		sql,
		taskID) // $1
	if err != nil {
		return nil, err
	}

	return &taskToDelete, nil
}

func getTasksAfterTaskToDelete(tx *sqlx.Tx, taskToDelete TaskToDelete) ([]TaskOrder, error) {
	sql := `SELECT id,
				   group_order
			FROM task
			WHERE customer_id = $1
			AND date = $2
			AND group_id = $3
			AND group_order > $4`

	tasks := []TaskOrder{}
	err := tx.Select(
		&tasks,
		sql,
		taskToDelete.CustomerID, // $1
		taskToDelete.Date,       // $2
		taskToDelete.GroupID,    // $3
		taskToDelete.GroupOrder) // $4
	if err != nil {
		return nil, err
	}

	return tasks, nil
}

func deleteTask(tx *sqlx.Tx, taskID int) error {
	sql := "DELETE FROM task WHERE id = :id"
	params := map[string]interface{}{
		"id": taskID,
	}

	_, err := tx.NamedExec(sql, params)
	if err != nil {
		return err
	}

	return nil
}
