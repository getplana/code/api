package taskgroups

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/txerrors"
	"gitlab.com/getplana/api/utils/update"
)

func Reorder(db *sqlx.DB, groupID int, newDayOrder int) error {
	groupToReorder, err := getTaskGroupToReorder(db, groupID)
	if err != nil {
		err = fmt.Errorf("getting task group to reorder: %s", err)
		return err
	}

	tx := db.MustBegin()
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	oldDayOrder := groupToReorder.DayOrder
	err = reorderTaskGroup(tx, *groupToReorder, oldDayOrder, newDayOrder)
	if err != nil {
		return err
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return err
	}

	return nil
}

func getTaskGroupToReorder(db *sqlx.DB, groupID int) (*TaskGroupToReorder, error) {
	sql := `SELECT id, 
				   customer_id,
				   date,
				   day_order
			FROM task_group
			WHERE id = $1`

	group := TaskGroupToReorder{}
	err := db.Get(
		&group,
		sql,
		groupID) // $1
	if err != nil {
		return nil, err
	}

	return &group, nil
}

func reorderTaskGroup(
	tx *sqlx.Tx,
	groupToReorder TaskGroupToReorder,
	oldDayOrder int,
	newDayOrder int,
) error {
	groupID := groupToReorder.ID
	customerID := groupToReorder.CustomerID
	date := groupToReorder.Date

	var topDayOrder int
	var botDayOrder int
	var topInclusive bool
	var botInclusive bool
	var shiftChange int
	if oldDayOrder < newDayOrder {
		topDayOrder = oldDayOrder
		botDayOrder = newDayOrder
		topInclusive = false
		botInclusive = true
		shiftChange = -1
	} else if oldDayOrder > newDayOrder {
		topDayOrder = newDayOrder
		botDayOrder = oldDayOrder
		topInclusive = true
		botInclusive = false
		shiftChange = 1
	}

	groups, err := getTaskGroupsToShift(tx, customerID, date, topDayOrder, topInclusive, botDayOrder, botInclusive)
	if err != nil {
		err = txerrors.Error("getting task groups to shift", err)
		return err
	}

	for _, group := range groups {
		newDayOrder := group.DayOrder + shiftChange
		groupUpdate := TaskGroupUpdate{DayOrder: &newDayOrder}
		err = update.Update(tx, "task_group", group.GroupID, groupUpdate)
		if err != nil {
			context := fmt.Sprintf("moving task group %d from day order %d to %d", group.GroupID, group.DayOrder, newDayOrder)
			err = txerrors.Error(context, err)
			return err
		}
	}

	// Update day order after shift goals to avoid also shifting goal to be moved.
	groupUpdate := TaskGroupUpdate{DayOrder: &newDayOrder}
	err = update.Update(tx, "task_group", groupID, groupUpdate)
	if err != nil {
		context := fmt.Sprintf("moving task group %d from day orde %d to %d", groupID, oldDayOrder, newDayOrder)
		err = txerrors.Error(context, err)
		return err
	}

	return nil
}

func getTaskGroupsToShift(
	tx *sqlx.Tx,
	customerID int,
	date time.Time,
	topDayOrder int,
	topInclusive bool,
	botDayOrder int,
	botInclusive bool,
) ([]TaskGroupOrder, error) {
	sql := `SELECT id,
				   day_order
	  		FROM task_group
	  		WHERE customer_id = $1
   			AND date = $2
	  		AND $3 %s day_order
	  		AND day_order %s $4
   			ORDER BY day_order`

	topOperator := "<"
	if topInclusive {
		topOperator = "<="
	}

	botOperator := "<"
	if botInclusive {
		botOperator = "<="
	}

	sql = fmt.Sprintf(sql, topOperator, botOperator)
	groups := []TaskGroupOrder{}
	err := tx.Select(
		&groups,
		sql,
		customerID,  // $1
		date,        // $2
		topDayOrder, // $3
		botDayOrder) // $4
	if err != nil {
		return nil, err
	}

	return groups, nil
}
