package taskgroups

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

func Create(db *sqlx.DB, newTaskGroup NewTaskGroup) (*TaskGroup, error) {
	sql := `
	WITH inserted_group AS (
		INSERT INTO task_group (
			customer_id,
			date,
			name,
			day_order
		) VALUES (
			$1,
			$2,
			$3,
			$4
		) RETURNING *
	)
	SELECT
		ig.id,
		ig.name,
		ig.day_order
	FROM inserted_group AS ig`

	createdGroup := TaskGroup{}
	err := db.Get(
		&createdGroup,
		sql,
		newTaskGroup.CustomerID, // $1
		newTaskGroup.Date,       // $2
		newTaskGroup.Name,       // $3
		newTaskGroup.DayOrder)   // $4
	if err != nil {
		err = fmt.Errorf("creating new task group: %s", err)
		return nil, err
	}

	return &createdGroup, nil
}
