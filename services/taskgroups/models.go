package taskgroups

import (
	"time"

	"gitlab.com/getplana/api/types"
)

type TaskGroup struct {
	ID       int    `db:"id"        json:"id"`
	Name     string `db:"name"      json:"name"`
	DayOrder int    `db:"day_order" json:"day_order"`
}

type NewTaskGroup struct {
	CustomerID *int            `json:"customer_id"`
	Date       *types.DateTime `json:"date"`
	Name       *string         `json:"name"`
	DayOrder   *int            `json:"day_order"`
}

type TaskGroupUpdate struct {
	Name     *string `json:"name"`
	DayOrder *int    `json:"day_order"`
}

type TaskGroupToReorder struct {
	ID         int       `db:"id"`
	CustomerID int       `db:"customer_id"`
	Date       time.Time `db:"date"`
	DayOrder   int       `db:"day_order"`
}

type TaskGroupToDelete struct {
	CustomerID int       `db:"customer_id"`
	Date       time.Time `db:"date"`
	DayOrder   int       `db:"day_order"`
}

type TaskGroupOrder struct {
	GroupID  int `db:"id"`
	DayOrder int `db:"day_order"`
}
