package tests

import (
	"fmt"
	"testing"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/getplana/api/services/taskgroups"
	"gitlab.com/getplana/api/types"
	"gitlab.com/getplana/api/utils/testutil"
	"gitlab.com/getplana/api/utils/timeutil"
)

func TestTaskGroupCreate(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("basic create", func(t *testing.T) {
		seedBasicCreate(db)

		customerID := 1
		date := types.DateTime(timeutil.CreateYMDTime("2021-01-31"))
		name := "Group 1"
		dayOrder := 1
		newTaskGroup := taskgroups.NewTaskGroup{
			CustomerID: &customerID,
			Date:       &date,
			Name:       &name,
			DayOrder:   &dayOrder,
		}

		_, err := taskgroups.Create(db, newTaskGroup)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		groupID := 1
		createdGroup, err := getTaskGroup(db, groupID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, customerID, createdGroup.CustomerID)
		assert.True(t, createdGroup.Date.Equal(time.Time(date)))
		assert.Equal(t, name, createdGroup.Name)
		assert.Equal(t, dayOrder, createdGroup.DayOrder)
	})
}

func seedBasicCreate(db *sqlx.DB) {
	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
	})
}

func getTaskGroup(db *sqlx.DB, groupID int) (*TaskGroup, error) {
	selectTaskGroup, _, err := SelectTaskGroups.
		Where(goqu.Ex{"id": groupID}).
		ToSQL()
	if err != nil {
		return nil, err
	}

	taskGroup := TaskGroup{}
	err = db.Get(&taskGroup, selectTaskGroup)
	if err != nil {
		return nil, err
	}

	return &taskGroup, nil
}
