package tests

import (
	"fmt"
	"testing"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/getplana/api/services/taskgroups"
	"gitlab.com/getplana/api/utils/testutil"
	"gitlab.com/getplana/api/utils/timeutil"
)

func TestTaskGroupUpdate(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("basic update", func(t *testing.T) {
		seedBasicUpdate(db)

		groupID := 1
		newName := "Group A"
		taskGroupUpdate := taskgroups.TaskGroupUpdate{Name: &newName}
		err := taskgroups.Update(db, groupID, taskGroupUpdate)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		taskGroup, err := getTaskGroup(db, groupID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, newName, taskGroup.Name)
	})
}

func seedBasicUpdate(db *sqlx.DB) {
	insertTaskGroup := goqu.Insert("task_group").Rows(
		TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "Group 1",
			DayOrder:   1,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		insertTaskGroup,
	})
}
