package tests

import (
	"fmt"
	"testing"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/getplana/api/services/taskgroups"
	"gitlab.com/getplana/api/utils/testutil"
	"gitlab.com/getplana/api/utils/timeutil"
)

func TestTaskGroupReorder(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("move top group to bottom", func(t *testing.T) {
		seedReorder(db)

		groupID := 1
		newDayOrder := 3
		err := taskgroups.Reorder(db, groupID, newDayOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		taskGroups, err := getTaskGroups(db)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 3, taskGroups[0].DayOrder)
		assert.Equal(t, 1, taskGroups[1].DayOrder)
		assert.Equal(t, 2, taskGroups[2].DayOrder)
	})

	t.Run("move bottom group to top", func(t *testing.T) {
		seedReorder(db)

		groupID := 3
		newDayOrder := 1
		err := taskgroups.Reorder(db, groupID, newDayOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		taskGroups, err := getTaskGroups(db)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 2, taskGroups[0].DayOrder)
		assert.Equal(t, 3, taskGroups[1].DayOrder)
		assert.Equal(t, 1, taskGroups[2].DayOrder)
	})

	t.Run("move group down one", func(t *testing.T) {
		seedReorder(db)

		groupID := 1
		newDayOrder := 2
		err := taskgroups.Reorder(db, groupID, newDayOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		taskGroups, err := getTaskGroups(db)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 2, taskGroups[0].DayOrder)
		assert.Equal(t, 1, taskGroups[1].DayOrder)
		assert.Equal(t, 3, taskGroups[2].DayOrder)
	})

	t.Run("move group up one", func(t *testing.T) {
		seedReorder(db)

		groupID := 3
		newDayOrder := 2
		err := taskgroups.Reorder(db, groupID, newDayOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		taskGroups, err := getTaskGroups(db)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 1, taskGroups[0].DayOrder)
		assert.Equal(t, 3, taskGroups[1].DayOrder)
		assert.Equal(t, 2, taskGroups[2].DayOrder)
	})
}

func seedReorder(db *sqlx.DB) {
	insertTaskGroups := goqu.Insert("task_group").Rows(
		TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "Group 1",
			DayOrder:   1,
		},
		TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "Group 2",
			DayOrder:   2,
		},
		TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "Group 3",
			DayOrder:   3,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		insertTaskGroups,
	})
}

func getTaskGroups(db *sqlx.DB) ([]taskgroups.TaskGroupOrder, error) {
	selectTaskGroupOrders, _, err := SelectTaskGroupOrders.
		Order(goqu.C("id").Asc()).
		ToSQL()
	if err != nil {
		return nil, err
	}

	taskGroups := []taskgroups.TaskGroupOrder{}
	err = db.Select(&taskGroups, selectTaskGroupOrders)
	if err != nil {
		return nil, err
	}

	return taskGroups, nil
}
