package tests

import (
	"fmt"
	"testing"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	goaltest "gitlab.com/getplana/api/services/goals/tests"
	"gitlab.com/getplana/api/services/taskgroups"
	"gitlab.com/getplana/api/services/tasks"
	tasktest "gitlab.com/getplana/api/services/tasks/tests"
	"gitlab.com/getplana/api/utils/testutil"
	"gitlab.com/getplana/api/utils/timeutil"
)

func TestTaskGroupDelete(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("basic delete", func(t *testing.T) {
		seedBasicDelete(db)

		groupID := 1
		err := taskgroups.Delete(db, groupID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		taskGroups, err := getTaskGroups(db)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 0, len(taskGroups))
	})

	t.Run("delete from top", func(t *testing.T) {
		seedDeleteFromTop(db)

		groupID := 1
		err := taskgroups.Delete(db, groupID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		taskGroups, err := getTaskGroups(db)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 2, len(taskGroups))

		assert.Equal(t, 2, taskGroups[0].GroupID)
		assert.Equal(t, 1, taskGroups[0].DayOrder)

		assert.Equal(t, 3, taskGroups[1].GroupID)
		assert.Equal(t, 2, taskGroups[1].DayOrder)
	})

	t.Run("tasks also deleted", func(t *testing.T) {
		seedTasksAlsoDeleted(db)

		groupID := 1
		err := taskgroups.Delete(db, groupID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		tasks, err := getTasks(db)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 0, len(tasks))
	})
}

func seedBasicDelete(db *sqlx.DB) {
	insertTaskGroup := goqu.Insert("task_group").Rows(
		TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "",
			DayOrder:   1,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		insertTaskGroup,
	})
}

func seedDeleteFromTop(db *sqlx.DB) {
	insertTaskGroups := goqu.Insert("task_group").Rows(
		TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "",
			DayOrder:   1,
		},
		TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "",
			DayOrder:   2,
		},
		TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "",
			DayOrder:   3,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		insertTaskGroups,
	})
}

func seedTasksAlsoDeleted(db *sqlx.DB) {
	insertTaskGroup := goqu.Insert("task_group").Rows(
		TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "",
			DayOrder:   1,
		},
	)

	groupID := 1
	insertTasks := goqu.Insert("task").Rows(
		tasktest.Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    groupID,
			GroupOrder: 1,
			Text:       "",
			GoalID:     1,
		},
		tasktest.Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    groupID,
			GroupOrder: 2,
			Text:       "",
			GoalID:     1,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		goaltest.InsertGoal,
		insertTaskGroup,
		insertTasks,
	})
}

func getTasks(db *sqlx.DB) ([]tasks.TaskOrder, error) {
	selectTaskOrders, _, err := tasktest.SelectTaskOrders.ToSQL()
	if err != nil {
		return nil, err
	}

	tasks := []tasks.TaskOrder{}
	err = db.Select(&tasks, selectTaskOrders)
	if err != nil {
		return nil, err
	}

	return tasks, nil
}
