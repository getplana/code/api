package tests

import (
	"time"

	"github.com/doug-martin/goqu/v9"
	"gitlab.com/getplana/api/services/taskgroups"
	"gitlab.com/getplana/api/utils/timeutil"
)

type TaskGroup struct {
	CustomerID int       `db:"customer_id"`
	Date       time.Time `db:"date"`
	Name       string    `db:"name"`
	DayOrder   int       `db:"day_order"`
}

var (
	SelectTaskGroups      = goqu.Select(&TaskGroup{}).From("task_group")
	SelectTaskGroupOrders = goqu.Select(&taskgroups.TaskGroupOrder{}).From("task_group")

	InsertTaskGroup = goqu.Insert("task_group").Rows(
		TaskGroup{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			Name:       "",
			DayOrder:   1,
		},
	)
)
