package taskgroups

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/constants"
)

func Routes(db *sqlx.DB) *chi.Mux {
	router := chi.NewRouter()
	router.Post("/", HandlePost(db))
	router.Patch("/{group_id}", HandlePatch(db))

	return router
}

func HandlePost(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		newGroup := NewTaskGroup{}
		err := json.NewDecoder(r.Body).Decode(&newGroup)
		if err != nil {
			log.Printf("Error with decoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		createdGroup, err := Create(db, newGroup)
		if err != nil {
			log.Printf("Error with creating task group: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(createdGroup)
		if err != nil {
			log.Printf("Error with encoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}
	}
}

func HandlePatch(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		groupIDStr := chi.URLParam(r, "group_id")
		groupID, err := strconv.Atoi(groupIDStr)
		if err != nil {
			msg := fmt.Sprintf("Invalid group ID '%s'", groupIDStr)
			log.Printf("%s: %s", msg, err)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}

		groupUpdate := TaskGroupUpdate{}
		err = json.NewDecoder(r.Body).Decode(&groupUpdate)
		if err != nil {
			log.Printf("Error with decoding JSON: %s", err)
			http.Error(w, "Invalid body.", http.StatusInternalServerError)
			return
		}

		if groupUpdate.DayOrder != nil {
			err = Reorder(db, groupID, *groupUpdate.DayOrder)
			if err != nil {
				log.Printf("Error with reordering task group: %s", err)
				http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
				return
			}
		} else if groupUpdate.Name != nil {
			err = Update(db, groupID, groupUpdate)
			if err != nil {
				log.Printf("Error with updating task group: %s", err)
				http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
				return
			}
		}
	}
}
