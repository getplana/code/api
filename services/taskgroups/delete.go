package taskgroups

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/txerrors"
	"gitlab.com/getplana/api/utils/update"
)

func Delete(db *sqlx.DB, groupID int) error {
	tx := db.MustBegin()
	var err error
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	err = deleteTasksForTaskGroup(tx, groupID)
	if err != nil {
		err = txerrors.Error("deleting tasks for task group", err)
		return err
	}

	err = shiftUpGroupsAfterGroupToDelete(tx, groupID)
	if err != nil {
		err = txerrors.Error("shifting up task groups after group to delete", err)
		return err
	}

	err = deleteTaskGroup(tx, groupID)
	if err != nil {
		err = txerrors.Error("deleting task group", err)
		return err
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return err
	}

	return nil
}

func deleteTasksForTaskGroup(tx *sqlx.Tx, groupID int) error {
	sql := `DELETE FROM task
			WHERE group_id = :group_id`

	params := map[string]interface{}{
		"group_id": groupID,
	}

	_, err := tx.NamedExec(sql, params)
	if err != nil {
		return err
	}

	return nil
}

func shiftUpGroupsAfterGroupToDelete(tx *sqlx.Tx, groupID int) error {
	groupToDelete, err := getTaskGroupToDelete(tx, groupID)
	if err != nil {
		err = fmt.Errorf("getting task group to delete: %s", err)
		return err
	}

	groups, err := getGroupsBelowGroupToDelete(tx, *groupToDelete)
	if err != nil {
		err = fmt.Errorf("getting task groups below group to delete: %s", err)
		return err
	}

	for _, group := range groups {
		newDayOrder := group.DayOrder - 1
		groupUpdate := TaskGroupUpdate{DayOrder: &newDayOrder}
		err = update.Update(tx, "task_group", group.GroupID, groupUpdate)
		if err != nil {
			context := fmt.Sprintf("moving task group %d from day order %d to %d", group.GroupID, group.DayOrder, newDayOrder)
			err = txerrors.Error(context, err)
			return err
		}
	}

	return nil
}

func getTaskGroupToDelete(tx *sqlx.Tx, groupID int) (*TaskGroupToDelete, error) {
	sql := `SELECT customer_id,
				   date,
				   day_order
			FROM task_group
			WHERE id = $1`

	group := TaskGroupToDelete{}
	err := tx.Get(
		&group,
		sql,
		groupID) // $1
	if err != nil {
		return nil, err
	}

	return &group, nil
}

func getGroupsBelowGroupToDelete(tx *sqlx.Tx, groupToDelete TaskGroupToDelete) ([]TaskGroupOrder, error) {
	sql := `SELECT id,
				   day_order
			FROM task_group
			WHERE customer_id = $1
			AND date = $2
			AND day_order > $3`

	groups := []TaskGroupOrder{}
	err := tx.Select(
		&groups,
		sql,
		groupToDelete.CustomerID, // $1
		groupToDelete.Date,       // $2
		groupToDelete.DayOrder)   // $3
	if err != nil {
		return nil, err
	}

	return groups, nil
}

func deleteTaskGroup(tx *sqlx.Tx, groupID int) error {
	sql := `DELETE FROM task_group
			WHERE id = :id`

	params := map[string]interface{}{
		"id": groupID,
	}

	_, err := tx.NamedExec(sql, params)
	if err != nil {
		return err
	}

	return nil
}
