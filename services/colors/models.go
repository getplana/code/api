package colors

type Color struct {
	ID   int    `db:"id"         json:"id"`
	Name string `db:"name"       json:"name"`
	HSL
}

type HSL struct {
	Hue        int `db:"hue"        json:"hue"`
	Saturation int `db:"saturation" json:"saturation"`
	Lightness  int `db:"lightness"  json:"lightness"`
}
