package colors

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/constants"
)

func Routes(db *sqlx.DB) *chi.Mux {
	router := chi.NewRouter()
	router.Get("/", HandleGet(db))

	return router
}

func HandleGet(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		colors, err := Get(db)
		if err != nil {
			log.Printf("Error with getting colors: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(colors)
		if err != nil {
			log.Printf("Error with encoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}
	}
}
