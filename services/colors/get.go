package colors

import "github.com/jmoiron/sqlx"

func Get(db *sqlx.DB) ([]Color, error) {
	sql := `SELECT id,
				   name,
				   hue,
				   saturation,
				   lightness
			FROM color
			ORDER BY id`

	colors := []Color{}
	err := db.Select(&colors, sql)
	if err != nil {
		return nil, err
	}

	return colors, nil
}
