package logs

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/constants"
)

func Routes(db *sqlx.DB) *chi.Mux {
	router := chi.NewRouter()
	router.Post("/", HandlePost(db))

	return router
}

func HandlePost(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		newLog := NewLog{}
		err := json.NewDecoder(r.Body).Decode(&newLog)
		if err != nil {
			log.Printf("Error with decoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		newRelicStatusCode, err := Create(db, newLog)
		if err != nil {
			log.Printf("Error with creating log: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		w.WriteHeader(*newRelicStatusCode)
	}
}
