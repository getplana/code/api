package logs

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/constants"
)

func Create(db *sqlx.DB, newLog NewLog) (*int, error) {
	envVar := "NEW_RELIC_API_KEY"
	newRelicApiKey := os.Getenv(envVar)
	if newRelicApiKey == "" {
		err := fmt.Errorf("%s environment variable is not defined", envVar)
		return nil, err
	}

	body := map[string]interface{}{
		"timestamp": newLog.Timestamp,
		"level":     newLog.Level,
		"message":   newLog.Message,
	}

	newLogBytes, err := json.Marshal(body)
	if err != nil {
		err := fmt.Errorf("error with encoding new log: %s", err)
		return nil, err
	}

	req, err := http.NewRequest("POST", constants.NewRelicEndpoint, bytes.NewBuffer(newLogBytes))
	if err != nil {
		err := fmt.Errorf("error with creating request: %s", err)
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Api-Key", newRelicApiKey)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		err := fmt.Errorf("error with sending log to New Relic: %s", err)
		return nil, err
	}

	// https://pkg.go.dev/net/http#NewRequest:~:text=The%20client%20must%20close%20the%20response%20body%20when%20finished%20with%20it%3A
	defer resp.Body.Close()

	return &resp.StatusCode, nil
}
