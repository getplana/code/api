package logs

type NewLog struct {
	Timestamp int    `json:"timestamp"`
	Level     string `json:"level"`
	Message   string `json:"message"`
}
