package tests

import (
	"fmt"
	"testing"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/getplana/api/constants"
	"gitlab.com/getplana/api/services/goals"
	"gitlab.com/getplana/api/utils/testutil"
)

func TestGoalReorder(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("move top goal to bottom", func(t *testing.T) {
		seedReorder(db)

		goalID := 1
		newDayOrder := 3
		err := goals.Reorder(db, goalID, newDayOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		activeGoals, err := getGoals(db, constants.GoalStateActive)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 1, activeGoals[1].OrderNumber)
		assert.Equal(t, 2, activeGoals[2].OrderNumber)
		assert.Equal(t, 3, activeGoals[0].OrderNumber)
	})

	t.Run("move bottom goal to top", func(t *testing.T) {
		seedReorder(db)

		goalID := 3
		newDayOrder := 1
		err := goals.Reorder(db, goalID, newDayOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		reorderedGoals, err := getGoals(db, constants.GoalStateActive)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 1, reorderedGoals[2].OrderNumber)
		assert.Equal(t, 2, reorderedGoals[0].OrderNumber)
		assert.Equal(t, 3, reorderedGoals[1].OrderNumber)
	})

	t.Run("move goal down one", func(t *testing.T) {
		seedReorder(db)

		goalID := 1
		newDayOrder := 2
		err := goals.Reorder(db, goalID, newDayOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		reorderedGoals, err := getGoals(db, constants.GoalStateActive)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 1, reorderedGoals[1].OrderNumber)
		assert.Equal(t, 2, reorderedGoals[0].OrderNumber)
		assert.Equal(t, 3, reorderedGoals[2].OrderNumber)
	})

	t.Run("move goal up one", func(t *testing.T) {
		seedReorder(db)

		goalID := 3
		newDayOrder := 2
		err := goals.Reorder(db, goalID, newDayOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		reorderedGoals, err := getGoals(db, constants.GoalStateActive)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 1, reorderedGoals[0].OrderNumber)
		assert.Equal(t, 2, reorderedGoals[2].OrderNumber)
		assert.Equal(t, 3, reorderedGoals[1].OrderNumber)
	})

	t.Run("reordering active goals does *not* reorder archived goals", func(t *testing.T) {
		seedReorder(db)

		goalID := 1
		newDayOrder := 3
		err := goals.Reorder(db, goalID, newDayOrder)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		archivedGoals, err := getGoals(db, constants.GoalStateArchived)
		assert.Nil(t, err, fmt.Sprintf("%s", err))
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, 1, archivedGoals[0].OrderNumber)
		assert.Equal(t, 2, archivedGoals[1].OrderNumber)
		assert.Equal(t, 3, archivedGoals[2].OrderNumber)
	})
}

func seedReorder(db *sqlx.DB) {
	insertActiveGoals := goqu.Insert("goal").Rows(
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateActive,
			OrderNumber: 1,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateActive,
			OrderNumber: 2,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateActive,
			OrderNumber: 3,
		},
	)

	insertArchivedGoals := goqu.Insert("goal").Rows(
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateArchived,
			OrderNumber: 1,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateArchived,
			OrderNumber: 2,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateArchived,
			OrderNumber: 3,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		insertActiveGoals,
		insertArchivedGoals,
	})
}

func getGoals(db *sqlx.DB, state string) ([]goals.GoalOrder, error) {
	selectGoalOrders, _, err := SelectGoalOrders.
		Where(goqu.Ex{"state": state}).
		Order(goqu.C("id").Asc()).
		ToSQL()
	if err != nil {
		return nil, err
	}

	tasks := []goals.GoalOrder{}
	err = db.Select(&tasks, selectGoalOrders)
	if err != nil {
		return nil, err
	}

	return tasks, nil
}
