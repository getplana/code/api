package tests

import (
	"fmt"
	"testing"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/getplana/api/constants"
	"gitlab.com/getplana/api/services/goals"
	grouptest "gitlab.com/getplana/api/services/taskgroups/tests"
	tasktest "gitlab.com/getplana/api/services/tasks/tests"
	"gitlab.com/getplana/api/utils/testutil"
	"gitlab.com/getplana/api/utils/timeutil"
)

func TestGoalDelete(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("basic delete", func(t *testing.T) {
		seedBasicDelete(db)

		// Delete goal.
		goalID := 1
		err := goals.Delete(db, goalID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Generate SQL.
		selectGoal, _, err := SelectGoals.
			Where(goqu.Ex{"id": goalID}).
			ToSQL()
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Get deleted goal.
		deletedGoal := Goal{}
		err = db.Get(&deletedGoal, selectGoal)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test that state for deleted goal is updated.
		assert.Equal(t, constants.GoalStateDeleted, deletedGoal.State)
	})

	t.Run("delete from top", func(t *testing.T) {
		seedDeleteFromTop(db)

		// Delete goal.
		goalID := 1
		err := goals.Delete(db, goalID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Generate SQL.
		selectGoalOrders, _, err := SelectGoalOrders.
			Where(goqu.Ex{"state": constants.GoalStateActive}).
			ToSQL()
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Get order numbers of goals below deleted goal.
		goals := []goals.GoalOrder{}
		err = db.Select(&goals, selectGoalOrders)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test that order number for goals below deleted goal was decreased by 1.
		assert.Equal(t, 1, goals[0].OrderNumber)
		assert.Equal(t, 2, goals[1].OrderNumber)
	})

	t.Run("tasks also deleted", func(t *testing.T) {
		seedTasksAlsoDeleted(db)

		// Delete goal.
		goalID := 1
		err := goals.Delete(db, goalID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Generate SQL.
		selectTasks, _, err := tasktest.SelectTasks.ToSQL()
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Get deleted tasks.
		tasks := []tasktest.Task{}
		err = db.Select(&tasks, selectTasks)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test that tasks for deleted goal also deleted.
		assert.Equal(t, 0, len(tasks))
	})
}

func seedBasicDelete(db *sqlx.DB) {
	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		InsertGoal,
	})
}

func seedDeleteFromTop(db *sqlx.DB) {
	insertActiveGoals := goqu.Insert("goal").Rows(
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateActive,
			OrderNumber: 1,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateActive,
			OrderNumber: 2,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateActive,
			OrderNumber: 3,
		},
	)

	insertArchivedGoals := goqu.Insert("goal").Rows(
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateArchived,
			OrderNumber: 1,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateArchived,
			OrderNumber: 2,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateArchived,
			OrderNumber: 3,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		insertActiveGoals,
		insertArchivedGoals,
	})
}

func seedTasksAlsoDeleted(db *sqlx.DB) {
	insertTask := goqu.Insert("task").Rows(
		tasktest.Task{
			CustomerID: 1,
			Date:       timeutil.CreateYMDTime("2021-01-31"),
			GroupID:    1,
			GroupOrder: 1,
			Text:       "",
			GoalID:     1,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		InsertGoal,
		grouptest.InsertTaskGroup,
		insertTask,
	})
}
