package tests

import (
	"time"

	"github.com/doug-martin/goqu/v9"
	"gitlab.com/getplana/api/constants"
	"gitlab.com/getplana/api/services/goals"
)

type Goal struct {
	CustomerID  int       `db:"customer_id"`
	State       string    `db:"state"`
	Text        string    `db:"text"`
	ColorID     int       `db:"color_id"`
	OrderNumber int       `db:"order_number"`
	CreatedAt   time.Time `db:"created_at"`
}

var (
	SelectGoals      = goqu.Select(&Goal{}).From("goal")
	SelectGoalOrders = goqu.Select(&goals.GoalOrder{}).From("goal")
	InsertGoal       = goqu.Insert("goal").Rows(
		Goal{
			CustomerID:  1,
			State:       constants.GoalStateActive,
			Text:        "",
			ColorID:     1,
			OrderNumber: 1,
		},
	)
)
