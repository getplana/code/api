package tests

import (
	"fmt"
	"testing"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/getplana/api/constants"
	"gitlab.com/getplana/api/services/goals"
	"gitlab.com/getplana/api/utils/testutil"
)

const activeGoalOrder = 2
const archivedGoalOrder = 1

func TestGoalUpdateState(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("archiving goal", func(t *testing.T) {
		seedDB(db)

		// Archive 2nd active goal.
		goalID := 2
		newIsActive := false
		newState := constants.GoalStateArchived
		err := goals.UpdateState(db, goalID, newIsActive)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// --- Test archived goal --- //

		// Generate SQL.
		selectGoal, _, err := SelectGoals.
			Where(goqu.Ex{"id": goalID}).
			ToSQL()
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Get archived goal.
		archivedGoal := Goal{}
		err = db.Get(&archivedGoal, selectGoal)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test archived goal: state should be "archived" and order number should be 1.
		assert.Equal(t, newState, archivedGoal.State)
		assert.Equal(t, 1, archivedGoal.OrderNumber)

		// --- Test order numbers of other archived goals --- //

		// Generate SQL.
		selectGoalOrders, _, err := SelectGoalOrders.
			Where(goqu.Ex{"state": constants.GoalStateArchived}).
			Where(goqu.Ex{"id": goqu.Op{"neq": 1}}).
			ToSQL()
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Get other archived goals.
		goals := []goals.GoalOrder{}
		err = db.Select(&goals, selectGoalOrders)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test order numbers of other archived goals.
		assert.Equal(t, 2, goals[0].OrderNumber)
		assert.Equal(t, 3, goals[1].OrderNumber)
		assert.Equal(t, 4, goals[2].OrderNumber)

		// --- Test active goals below archived goal --- //

		// Generate SQL.
		selectGoalOrders, _, err = SelectGoalOrders.
			Where(goqu.Ex{"state": constants.GoalStateActive}).
			Where(goqu.C("order_number").Gt(activeGoalOrder)).
			ToSQL()
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Get active goals below archived goal.
		err = db.Select(&goals, selectGoalOrders)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test order numbers of active goals below archived goal.
		assert.Equal(t, 2, goals[0].OrderNumber)
		assert.Equal(t, 3, goals[1].OrderNumber)
	})

	t.Run("unarchiving goal", func(t *testing.T) {
		seedDB(db)

		// Unarchive 1st archived goal
		goalID := 5
		newIsActive := true
		newState := constants.GoalStateActive
		err := goals.UpdateState(db, goalID, newIsActive)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// --- Test unarchived goal --- //

		// Generate SQL.
		selectGoal, _, err := SelectGoals.
			Where(goqu.Ex{"id": goalID}).
			ToSQL()
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Get unarchived goal.
		unarchivedGoal := Goal{}
		err = db.Get(&unarchivedGoal, selectGoal)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test unarchived goal: state should be "active" and order number should be
		// numActiveGoals + 1.
		assert.Equal(t, newState, unarchivedGoal.State)
		assert.Equal(t, 5, unarchivedGoal.OrderNumber)

		// --- Test archived goals below unarchived goal --- //

		// Generate SQL.
		selectGoalOrders, _, err := SelectGoalOrders.
			Where(goqu.Ex{"state": constants.GoalStateArchived}).
			// Minus 1 because all of the archived goals already had their order numbers decreased
			// by 1.
			Where(goqu.C("order_number").Gt(archivedGoalOrder - 1)).
			ToSQL()
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Get archived goals below unarchived goal.
		goals := []goals.GoalOrder{}
		err = db.Select(&goals, selectGoalOrders)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test order numbers of archived goals below unarchived goal.
		assert.Equal(t, 1, goals[0].OrderNumber)
		assert.Equal(t, 2, goals[1].OrderNumber)
	})
}

func seedDB(db *sqlx.DB) {
	insertActiveGoals := goqu.Insert("goal").Rows(
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateActive,
			OrderNumber: 1,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateActive,
			OrderNumber: activeGoalOrder,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateActive,
			OrderNumber: 3,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateActive,
			OrderNumber: 4,
		},
	)

	insertArchivedGoals := goqu.Insert("goal").Rows(
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateArchived,
			OrderNumber: archivedGoalOrder,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateArchived,
			OrderNumber: 2,
		},
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateArchived,
			OrderNumber: 3,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		insertActiveGoals,
		insertArchivedGoals,
	})
}
