package tests

import (
	"fmt"
	"testing"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/getplana/api/constants"
	"gitlab.com/getplana/api/services/goals"
	"gitlab.com/getplana/api/utils/pointer"
	"gitlab.com/getplana/api/utils/testutil"
)

func TestGoalCreate(t *testing.T) {
	db := testutil.ConnectDB()

	t.Run("basic create", func(t *testing.T) {
		seedBasicCreate(db)

		// Initialize new goal.
		customerID := 1
		text := "text"
		colorID := 1
		newGoal := goals.NewGoal{
			CustomerID: &customerID,
			Text:       &text,
			ColorID:    &colorID,
		}

		// Create goal.
		_, err := goals.Create(db, newGoal)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Generate SQL.
		selectGoal, _, err := SelectGoals.ToSQL()
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Get created goal.
		createdGoal := Goal{}
		err = db.Get(&createdGoal, selectGoal)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test created goal.
		assert.Equal(t, customerID, createdGoal.CustomerID)
		assert.Equal(t, text, createdGoal.Text)
		assert.Equal(t, colorID, createdGoal.ColorID)
		assert.Equal(t, constants.GoalStateActive, createdGoal.State)
		assert.Equal(t, 1, createdGoal.OrderNumber)
	})

	t.Run("correct order number", func(t *testing.T) {
		seedOrderNumber(db)

		// Initialize new goal.
		newGoal := goals.NewGoal{
			CustomerID: pointer.Int(1),
			Text:       pointer.String("text"),
			ColorID:    pointer.Int(1),
		}

		// Create goal.
		_, err := goals.Create(db, newGoal)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Generate SQL.
		selectGoal, _, err := SelectGoals.
			Where(goqu.Ex{"id": 2}).
			ToSQL()
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Get created goal.
		createdGoal := Goal{}
		err = db.Get(&createdGoal, selectGoal)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		// Test order number of created goal.
		assert.Equal(t, 2, createdGoal.OrderNumber)
	})
}

func seedBasicCreate(db *sqlx.DB) {
	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
	})
}

func seedOrderNumber(db *sqlx.DB) {
	insertGoal := goqu.Insert("goal").Rows(
		Goal{
			CustomerID:  1,
			Text:        "",
			ColorID:     1,
			State:       constants.GoalStateActive,
			OrderNumber: 1,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		insertGoal,
	})
}
