package tests

import (
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/getplana/api/constants"
	"gitlab.com/getplana/api/services/goals"
	grouptest "gitlab.com/getplana/api/services/taskgroups/tests"
	tasktest "gitlab.com/getplana/api/services/tasks/tests"
	"gitlab.com/getplana/api/utils/testutil"
)

var (
	timeZoneStr = "America/New_York"
	timeZone, _ = time.LoadLocation(timeZoneStr)
	todayET     = time.Date(2022, 1, 31, 12, 0, 0, 0, timeZone)
	yesterdayET = time.Date(2022, 1, 30, 12, 0, 0, 0, timeZone)

	customerID     = 1
	oldGoalText    = "Old text"
	oldGoalColorID = 1
)

func TestGoalUpdate(t *testing.T) {
	goals.TimeNow = func() time.Time {
		return todayET
	}

	db := testutil.ConnectDB()

	t.Run("updating ACTIVE goal with created_at as today updates goal", func(t *testing.T) {
		goalState := constants.GoalStateActive
		seedToday(db, goalState)

		goalID := 1
		newText := "New text"
		newColorID := 2

		goalUpdate := goals.GoalUpdate{Text: &newText, ColorID: &newColorID}
		_, err := goals.Update(db, goalID, goalUpdate, timeZone)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		updatedGoal, err := getGoal(db, goalID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, newText, updatedGoal.Text)
		assert.Equal(t, newColorID, updatedGoal.ColorID)
	})

	t.Run("updating ARCHIVED goal with created_at as today updates goal", func(t *testing.T) {
		goalState := constants.GoalStateArchived
		seedToday(db, goalState)

		goalID := 1
		newText := "New text"
		newColorID := 2

		goalUpdate := goals.GoalUpdate{Text: &newText, ColorID: &newColorID}
		_, err := goals.Update(db, goalID, goalUpdate, timeZone)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		updatedGoal, err := getGoal(db, goalID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, newText, updatedGoal.Text)
		assert.Equal(t, newColorID, updatedGoal.ColorID)
	})

	t.Run("updating ACTIVE goal with created_at before today creates new goal", func(t *testing.T) {
		goalState := constants.GoalStateActive
		seedBeforeToday(db, goalState)

		goalID := 1
		newText := "New text"
		newColorID := 2
		newColorHue := testutil.Orange.Hue
		newColorSaturation := testutil.Orange.Saturation
		newColorLightness := testutil.Orange.Lightness

		goalUpdate := goals.GoalUpdate{Text: &newText, ColorID: &newColorID}
		createdGoal, err := goals.Update(db, goalID, goalUpdate, timeZone)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, newText, createdGoal.Text)
		assert.Equal(t, newColorHue, createdGoal.Hue)
		assert.Equal(t, newColorSaturation, createdGoal.Saturation)
		assert.Equal(t, newColorLightness, createdGoal.Lightness)

		newGoalID := 2
		newGoal, err := getGoal(db, newGoalID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, newText, newGoal.Text)
		assert.Equal(t, newColorID, newGoal.ColorID)
	})

	t.Run("updating ACTIVE goal with created_at before today does *not* update old goal", func(t *testing.T) {
		goalState := constants.GoalStateActive
		seedBeforeToday(db, goalState)

		goalID := 1
		newText := "New text"
		newColorID := 2

		goalUpdate := goals.GoalUpdate{Text: &newText, ColorID: &newColorID}
		_, err := goals.Update(db, goalID, goalUpdate, timeZone)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		oldGoal, err := getGoal(db, goalID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, oldGoalText, oldGoal.Text)
		assert.Equal(t, oldGoalColorID, oldGoal.ColorID)
	})

	t.Run("updating ACTIVE goal with created_at before today updates goal for today tasks from old to new goal", func(t *testing.T) {
		goalState := constants.GoalStateActive
		seedBeforeTodayWithTasks(db, goalState)

		goalID := 1
		newText := "New text"
		newColorID := 2

		goalUpdate := goals.GoalUpdate{Text: &newText, ColorID: &newColorID}
		_, err := goals.Update(db, goalID, goalUpdate, timeZone)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		taskID := 1
		task, err := getTask(db, taskID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		newGoalID := 2
		assert.Equal(t, newGoalID, task.GoalID)
	})

	t.Run("updating ACTIVE goal with created_at before today does *not* update goal for tasks before today", func(t *testing.T) {
		goalState := constants.GoalStateActive
		seedBeforeTodayWithTasks(db, goalState)

		goalID := 1
		newText := "New text"
		newColorID := 2

		goalUpdate := goals.GoalUpdate{Text: &newText, ColorID: &newColorID}
		_, err := goals.Update(db, goalID, goalUpdate, timeZone)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		taskID := 2
		task, err := getTask(db, taskID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, goalID, task.GoalID)
	})

	t.Run("updating ARCHIVED goal with created_at before today creates new goal", func(t *testing.T) {
		goalState := constants.GoalStateArchived
		seedBeforeToday(db, goalState)

		goalID := 1
		newText := "New text"
		newColorID := 2
		newColorHue := testutil.Orange.Hue
		newColorSaturation := testutil.Orange.Saturation
		newColorLightness := testutil.Orange.Lightness

		goalUpdate := goals.GoalUpdate{Text: &newText, ColorID: &newColorID}
		createdGoal, err := goals.Update(db, goalID, goalUpdate, timeZone)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, newText, createdGoal.Text)
		assert.Equal(t, newColorHue, createdGoal.Hue)
		assert.Equal(t, newColorSaturation, createdGoal.Saturation)
		assert.Equal(t, newColorLightness, createdGoal.Lightness)

		newGoalID := 2
		newGoal, err := getGoal(db, newGoalID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, newText, newGoal.Text)
		assert.Equal(t, newColorID, newGoal.ColorID)
	})

	t.Run("updating ARCHIVED goal with created_at before today does *not* update old goal", func(t *testing.T) {
		goalState := constants.GoalStateArchived
		seedBeforeToday(db, goalState)

		goalID := 1
		newText := "New text"
		newColorID := 2

		goalUpdate := goals.GoalUpdate{Text: &newText, ColorID: &newColorID}
		_, err := goals.Update(db, goalID, goalUpdate, timeZone)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		oldGoal, err := getGoal(db, goalID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, oldGoalText, oldGoal.Text)
		assert.Equal(t, oldGoalColorID, oldGoal.ColorID)
	})

	t.Run("updating ARCHIVED goal with created_at before today does *not* update goals for today tasks", func(t *testing.T) {
		goalState := constants.GoalStateArchived
		seedBeforeTodayWithTasks(db, goalState)

		goalID := 1
		newText := "New text"
		newColorID := 2

		goalUpdate := goals.GoalUpdate{Text: &newText, ColorID: &newColorID}
		_, err := goals.Update(db, goalID, goalUpdate, timeZone)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		taskID := 1
		task, err := getTask(db, taskID)
		assert.Nil(t, err, fmt.Sprintf("%s", err))

		assert.Equal(t, goalID, task.GoalID)
	})
}

func seedToday(db *sqlx.DB, goalState string) {
	insertCustomerAndGoal(db, goalState, todayET)
}

func seedBeforeToday(db *sqlx.DB, goalState string) {
	insertCustomerAndGoal(db, goalState, yesterdayET)
}

func seedBeforeTodayWithTasks(db *sqlx.DB, goalState string) {
	insertCustomerAndGoal(db, goalState, yesterdayET)

	insertTask := goqu.Insert("task").Rows(
		tasktest.Task{
			CustomerID: customerID,
			Date:       todayET,
			GroupID:    1,
			GroupOrder: 1,
			Text:       "",
			GoalID:     1,
		},
		tasktest.Task{
			CustomerID: customerID,
			Date:       yesterdayET,
			GroupID:    1,
			GroupOrder: 1,
			Text:       "",
			GoalID:     1,
		},
	)

	err := testutil.ExecInserts(db, []*goqu.InsertDataset{
		grouptest.InsertTaskGroup,
		insertTask,
	})
	if err != nil {
		log.Fatalf("Error with inserting task: %s", err)
	}
}

func insertCustomerAndGoal(db *sqlx.DB, goalState string, goalCreatedAtInTimeZone time.Time) {
	createdAtInUTC := goalCreatedAtInTimeZone.In(time.UTC)
	insertGoal := goqu.Insert("goal").Rows(
		Goal{
			CustomerID:  customerID,
			Text:        oldGoalText,
			ColorID:     oldGoalColorID,
			State:       goalState,
			OrderNumber: 1,
			CreatedAt:   createdAtInUTC,
		},
	)

	testutil.ClearAndSeedDB(db, []*goqu.InsertDataset{
		testutil.InsertCustomer,
		insertGoal,
	})
}

func getGoal(db *sqlx.DB, goalID int) (*Goal, error) {
	selectGoal, _, err := SelectGoals.
		Where(goqu.Ex{"id": goalID}).
		ToSQL()
	if err != nil {
		return nil, err
	}

	goal := Goal{}
	err = db.Get(&goal, selectGoal)
	if err != nil {
		return nil, err
	}

	return &goal, nil
}

func getTask(db *sqlx.DB, taskID int) (*tasktest.Task, error) {
	selectTask, _, err := tasktest.SelectTasks.
		Where(goqu.Ex{"id": taskID}).
		ToSQL()
	if err != nil {
		return nil, err
	}

	task := tasktest.Task{}
	err = db.Get(&task, selectTask)
	if err != nil {
		return nil, err
	}

	return &task, nil
}
