package goals

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/constants"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/pointer"
	"gitlab.com/getplana/api/utils/txerrors"
	"gitlab.com/getplana/api/utils/update"
)

func UpdateState(db *sqlx.DB, goalID int, newIsActive bool) error {
	goalToUpdate, err := getGoalToUpdateState(db, goalID)
	if err != nil {
		err = fmt.Errorf("getting goal to update: %s", err)
		return err
	}

	customerID := goalToUpdate.CustomerID
	orderNumber := goalToUpdate.OrderNumber

	tx := db.MustBegin()
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	isArchiving := !newIsActive
	shiftDown := 1
	shiftUp := -1
	if isArchiving {
		err := shiftGoals(tx, customerID, constants.GoalStateArchived, 0, shiftDown)
		if err != nil {
			err = txerrors.Error("shifting down archived goals", err)
			return err
		}

		err = shiftGoals(tx, customerID, constants.GoalStateActive, orderNumber, shiftUp)
		if err != nil {
			err = txerrors.Error("shifting up active goals below goal to archive", err)
			return err
		}

		goalUpdate := GoalUpdate{OrderNumber: pointer.Int(1)}
		err = update.Update(tx, "goal", goalID, goalUpdate)
		if err != nil {
			err = txerrors.Error("setting order number for goal to archive to 1", err)
			return err
		}
	} else if !isArchiving {
		err = shiftGoals(tx, customerID, constants.GoalStateArchived, orderNumber, shiftUp)
		if err != nil {
			err = txerrors.Error("shifting up archived goals below goal to unarchive", err)
			return err
		}

		activeGoals, err := getGoals(tx, customerID, constants.GoalStateActive, 0)
		if err != nil {
			err = txerrors.Error("getting active goals", err)
			return err
		}

		goalUpdate := GoalUpdate{OrderNumber: pointer.Int(len(activeGoals) + 1)}
		err = update.Update(tx, "goal", goalID, goalUpdate)
		if err != nil {
			err = txerrors.Error("setting order number for goal to unarchive to end", err)
			return err
		}
	}

	err = updateGoalState(tx, goalID, newIsActive)
	if err != nil {
		err = txerrors.Error("updating goal's state", err)
		return err
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return err
	}

	return nil
}

func getGoalToUpdateState(db *sqlx.DB, goalID int) (*GoalToUpdateState, error) {
	sql := `SELECT customer_id,
				   order_number
			FROM goal
			WHERE id = $1`

	goal := GoalToUpdateState{}
	err := db.Get(
		&goal,
		sql,
		goalID) // $1
	if err != nil {
		return nil, err
	}

	return &goal, nil
}

func shiftGoals(tx *sqlx.Tx, customerID int, state string, orderNumber int, shiftChange int) error {
	goals, err := getGoals(tx, customerID, state, orderNumber)
	if err != nil {
		err = fmt.Errorf("getting goals: %s", err)
		return err
	}

	for _, goal := range goals {
		newOrderNumber := goal.OrderNumber + shiftChange
		goalUpdate := GoalUpdate{OrderNumber: &newOrderNumber}
		err = update.Update(tx, "goal", goal.GoalID, goalUpdate)
		if err != nil {
			context := fmt.Sprintf("moving goal %d from order number %d to %d", goal.GoalID, goal.OrderNumber, newOrderNumber)
			err = txerrors.Error(context, err)
			return err
		}
	}

	return nil
}

func getGoals(tx *sqlx.Tx, customerID int, state string, orderNumber int) ([]GoalOrder, error) {
	sql := `SELECT id,
				   order_number
			FROM goal
			WHERE customer_id = $1
			AND state = $2::text
			AND order_number > $3`

	goals := []GoalOrder{}
	err := tx.Select(
		&goals,
		sql,
		customerID,  // $1
		state,       // $2
		orderNumber) // $3
	if err != nil {
		return nil, err
	}

	return goals, nil
}

func updateGoalState(tx *sqlx.Tx, goalID int, isActive bool) error {
	sql := `UPDATE goal
		    SET state = :state
		    WHERE id = :id`

	newState := getGoalState(isActive)
	params := map[string]interface{}{
		"id":    goalID,
		"state": newState,
	}

	_, err := tx.NamedExec(sql, params)
	if err != nil {
		return err
	}

	return nil
}

func getGoalState(isActive bool) string {
	if isActive {
		return constants.GoalStateActive
	} else {
		return constants.GoalStateArchived
	}
}
