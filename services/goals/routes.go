package goals

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/constants"
)

func Routes(db *sqlx.DB) *chi.Mux {
	router := chi.NewRouter()
	router.Get("/", HandleGet(db))
	router.Post("/", HandlePost(db))
	router.Patch("/{goal_id}", HandlePatch(db))
	router.Delete("/{goal_id}", HandleDelete(db))

	return router
}

func HandleGet(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		customerIDStr := r.URL.Query().Get("customer_id")
		id, err := strconv.Atoi(customerIDStr)
		if err != nil {
			log.Printf("Invalid customer_id '%s': %s", customerIDStr, err)
			http.Error(w, "Invalid customer_id.", http.StatusBadRequest)
			return
		}

		isActiveStr := r.URL.Query().Get("is_active")
		isActive, err := strconv.ParseBool(isActiveStr)
		if err != nil {
			log.Printf("Invalid is_active '%s': %s", isActiveStr, err)
			http.Error(w, "Invalid is_active.", http.StatusBadRequest)
			return
		}

		goals, err := Get(db, id, isActive)
		if err != nil {
			log.Printf("Error with getting goals: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(goals)
		if err != nil {
			log.Printf("Error with encoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}
	}
}

func HandlePost(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		newGoal := NewGoal{}
		err := json.NewDecoder(r.Body).Decode(&newGoal)
		if err != nil {
			log.Printf("Error with decoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		createdGoal, err := Create(db, newGoal)
		if err != nil {
			log.Printf("Error with creating goal: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(createdGoal)
		if err != nil {
			log.Printf("Error with encoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}
	}
}

func HandlePatch(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		goalIDStr := chi.URLParam(r, "goal_id")
		goalID, err := strconv.Atoi(goalIDStr)
		if err != nil {
			log.Printf("Invalid goal_id '%s': %s", goalIDStr, err)
			http.Error(w, "Invalid goal_id.", http.StatusBadRequest)
			return
		}

		goalUpdate := GoalUpdate{}
		err = json.NewDecoder(r.Body).Decode(&goalUpdate)
		if err != nil {
			log.Printf("Error with decoding JSON: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}

		if goalUpdate.OrderNumber != nil {
			err = Reorder(db, goalID, *goalUpdate.OrderNumber)
			if err != nil {
				log.Printf("Error with reordering goal: %s", err)
				http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
				return
			}
		} else if goalUpdate.IsActive != nil {
			err = UpdateState(db, goalID, *goalUpdate.IsActive)
			if err != nil {
				log.Printf("Error with updating goal's state: %s", err)
				http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
				return
			}
		} else {
			// NOTE: The cleaner solution would be to store the customer's time zone in the
			// database. However, having the PATCH goals endpoint take a query parameter for the
			// customer's time zone is faster and okay for now.

			customerTimeZoneStr := r.URL.Query().Get("time_zone")
			if err != nil {
				http.Error(w, "time_zone is missing.", http.StatusBadRequest)
				return
			}

			customerTimeZone, err := time.LoadLocation(customerTimeZoneStr)
			if err != nil {
				log.Printf("Error with loading customer's time zone: %s", err)
				http.Error(w, "Invalid time_zone.", http.StatusBadRequest)
				return
			}

			createdGoal, err := Update(db, goalID, goalUpdate, customerTimeZone)
			if err != nil {
				log.Printf("Error with updating goal: %s", err)
				http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
				return
			}

			err = json.NewEncoder(w).Encode(createdGoal)
			if err != nil {
				log.Printf("Error with encoding JSON: %s", err)
				http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
				return
			}
		}
	}
}

func HandleDelete(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		goalIDStr := chi.URLParam(r, "goal_id")
		goalID, err := strconv.Atoi(goalIDStr)
		if err != nil {
			log.Printf("Invalid goal_id '%s': %s", goalIDStr, err)
			http.Error(w, "Invalid goal_id.", http.StatusBadRequest)
			return
		}

		err = Delete(db, goalID)
		if err != nil {
			log.Printf("Error with deleting goal: %s", err)
			http.Error(w, constants.InternalServerError, http.StatusInternalServerError)
			return
		}
	}
}
