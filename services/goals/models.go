package goals

import (
	"time"

	"gitlab.com/getplana/api/services/colors"
)

type Goal struct {
	ID          int    `db:"id"           json:"id"`
	Text        string `db:"text"         json:"text"`
	OrderNumber int    `db:"order_number" json:"order_number"`
	colors.HSL  `json:"color"`
}

type NewGoal struct {
	CustomerID  *int    `json:"customer_id"`
	Text        *string `json:"text"`
	ColorID     *int    `json:"color_id"`
	State       *string
	OrderNumber *int
}

type GoalUpdate struct {
	Text        *string `json:"text"`
	ColorID     *int    `json:"color_id"`
	OrderNumber *int    `json:"order_number"`
	IsActive    *bool   `json:"is_active"`
}

type GoalToUpdate struct {
	CustomerID  int       `db:"customer_id"`
	State       string    `db:"state"`
	Text        string    `db:"text"`
	ColorID     int       `db:"color_id"`
	OrderNumber int       `db:"order_number"`
	CreatedAt   time.Time `db:"created_at"`
}

type GoalToUpdateState struct {
	CustomerID  int `db:"customer_id"`
	OrderNumber int `db:"order_number"`
}

type GoalToReorder struct {
	ID          int    `db:"id"`
	CustomerID  int    `db:"customer_id"`
	State       string `db:"state"`
	OrderNumber int    `db:"order_number"`
}

type GoalToDelete struct {
	CustomerID  int    `db:"customer_id"`
	State       string `db:"state"`
	OrderNumber int    `db:"order_number"`
}

type GoalOrder struct {
	GoalID      int `db:"id"`
	OrderNumber int `db:"order_number"`
}
