package goals

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/constants"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/txerrors"
	"gitlab.com/getplana/api/utils/update"
)

var TimeNow = time.Now

func Update(db *sqlx.DB, goalID int, goalUpdate GoalUpdate, customerTimeZone *time.Location) (*Goal, error) {
	goalToUpdate, err := getGoalToUpdate(db, goalID)
	if err != nil {
		err = fmt.Errorf("getting goal to update: %s", err)
		return nil, err
	}

	var createdGoal *Goal
	tx := db.MustBegin()
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	// Was the goal created today in the customer's time zone?
	if isTodayInTimeZone(goalToUpdate.CreatedAt, customerTimeZone) {
		err := updateGoal(tx, goalID, goalUpdate)
		if err != nil {
			context := fmt.Sprintf("updating goal %d", goalID)
			err = txerrors.Error(context, err)
			return nil, err
		}
	} else {
		goalText := goalUpdate.Text
		if goalText == nil {
			goalText = &goalToUpdate.Text
		}

		goalColorID := goalUpdate.ColorID
		if goalColorID == nil {
			goalColorID = &goalToUpdate.ColorID
		}

		newGoal := NewGoal{
			CustomerID:  &goalToUpdate.CustomerID,
			Text:        goalText,
			ColorID:     goalColorID,
			State:       &goalToUpdate.State,
			OrderNumber: &goalToUpdate.OrderNumber,
		}

		createdGoal, err = createGoal(tx, newGoal)
		if err != nil {
			err = txerrors.Error("creating goal", err)
			return nil, err
		}

		// Archived goals should not have any tasks for today.
		if goalToUpdate.State == constants.GoalStateActive {
			err = updateGoalForTodayTasks(tx, goalID, createdGoal.ID, customerTimeZone)
			if err != nil {
				err = txerrors.Error("updating goal for today tasks", err)
				return nil, err
			}
		}
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return nil, err
	}

	return createdGoal, nil
}

func getGoalToUpdate(db *sqlx.DB, goalID int) (*GoalToUpdate, error) {
	sql := `SELECT customer_id,
				   state,
				   text,
				   color_id,
				   order_number,
				   created_at
			FROM goal
			WHERE id = $1`

	goal := GoalToUpdate{}
	err := db.Get(
		&goal,
		sql,
		goalID) // $1
	if err != nil {
		return nil, err
	}

	return &goal, nil
}

func isTodayInTimeZone(date time.Time, timeZone *time.Location) bool {
	dateInTimeZone := date.In(timeZone)
	todayInTimeZone := TimeNow().In(timeZone)

	return dateInTimeZone.Year() == todayInTimeZone.Year() &&
		dateInTimeZone.YearDay() == todayInTimeZone.YearDay()
}

func updateGoal(tx *sqlx.Tx, goalID int, goalUpdate GoalUpdate) error {
	err := update.Update(tx, "goal", goalID, goalUpdate)
	if err != nil {
		return err
	}

	return nil
}

func updateGoalForTodayTasks(tx *sqlx.Tx, oldGoalID int, newGoalID int, customerTimeZone *time.Location) error {
	sql := `UPDATE task
			SET goal_id = :new_goal_id
			WHERE goal_id = :old_goal_id
			AND date = :date`

	todayInTimeZone := TimeNow().In(customerTimeZone)
	today := todayInTimeZone.Format(constants.YMDLayout)

	params := map[string]interface{}{
		"new_goal_id": newGoalID,
		"old_goal_id": oldGoalID,
		"date":        today,
	}

	_, err := tx.NamedExec(sql, params)
	if err != nil {
		return err
	}

	return nil
}
