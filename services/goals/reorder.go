package goals

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/txerrors"
	"gitlab.com/getplana/api/utils/update"
)

func Reorder(db *sqlx.DB, goalID int, newOrderNumber int) error {
	goalToReorder, err := getGoalToReorder(db, goalID)
	if err != nil {
		err = fmt.Errorf("getting goal to reorder: %s", err)
		return err
	}

	tx := db.MustBegin()
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	oldOrderNumber := goalToReorder.OrderNumber
	err = reorderGoal(tx, *goalToReorder, oldOrderNumber, newOrderNumber)
	if err != nil {
		return err
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return err
	}

	return nil
}

func getGoalToReorder(db *sqlx.DB, goalID int) (*GoalToReorder, error) {
	sql := `SELECT id, 
				   customer_id,
				   state,
				   order_number
			FROM goal
			WHERE id = $1`

	goal := GoalToReorder{}
	err := db.Get(
		&goal,
		sql,
		goalID) // $1
	if err != nil {
		return nil, err
	}

	return &goal, nil
}

func reorderGoal(
	tx *sqlx.Tx,
	goalToReorder GoalToReorder,
	oldOrderNumber int,
	newOrderNumber int,
) error {
	goalID := goalToReorder.ID
	customerID := goalToReorder.CustomerID
	state := goalToReorder.State

	var topOrderNumber int
	var botOrderNumber int
	var topInclusive bool
	var botInclusive bool
	var shiftChange int
	if oldOrderNumber < newOrderNumber {
		topOrderNumber = oldOrderNumber
		botOrderNumber = newOrderNumber
		topInclusive = false
		botInclusive = true
		shiftChange = -1
	} else if oldOrderNumber > newOrderNumber {
		topOrderNumber = newOrderNumber
		botOrderNumber = oldOrderNumber
		topInclusive = true
		botInclusive = false
		shiftChange = 1
	}

	goals, err := getGoalsToShift(tx, customerID, state, topOrderNumber, topInclusive, botOrderNumber, botInclusive)
	if err != nil {
		err = txerrors.Error("getting goals to shift", err)
		return err
	}

	for _, goal := range goals {
		newOrderNumber := goal.OrderNumber + shiftChange
		goalUpdate := GoalUpdate{OrderNumber: &newOrderNumber}
		err = update.Update(tx, "goal", goal.GoalID, goalUpdate)
		if err != nil {
			context := fmt.Sprintf("moving goal %d from order number %d to %d", goal.GoalID, goal.OrderNumber, newOrderNumber)
			err = txerrors.Error(context, err)
			return err
		}
	}

	// Update order number after shift goals to avoid also shifting goal to be moved.
	goalUpdate := GoalUpdate{OrderNumber: &newOrderNumber}
	err = update.Update(tx, "goal", goalID, goalUpdate)
	if err != nil {
		context := fmt.Sprintf("moving goal %d from order number %d to %d", goalID, oldOrderNumber, newOrderNumber)
		err = txerrors.Error(context, err)
		return err
	}

	return nil
}

func getGoalsToShift(
	tx *sqlx.Tx,
	customerID int,
	state string,
	topOrderNumber int,
	topInclusive bool,
	botOrderNumber int,
	botInclusive bool,
) ([]GoalOrder, error) {
	sql := `SELECT id,
				   order_number
	  		FROM goal
	  		WHERE customer_id = $1
   			AND state = $2::text
	  		AND $3 %s order_number
	  		AND order_number %s $4
   			ORDER BY order_number`

	topOperator := "<"
	if topInclusive {
		topOperator = "<="
	}

	botOperator := "<"
	if botInclusive {
		botOperator = "<="
	}

	sql = fmt.Sprintf(sql, topOperator, botOperator)
	goals := []GoalOrder{}
	err := tx.Select(
		&goals,
		sql,
		customerID,     // $1
		state,          // $2
		topOrderNumber, // $3
		botOrderNumber) // $4
	if err != nil {
		return nil, err
	}

	return goals, nil
}
