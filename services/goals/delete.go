package goals

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/constants"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/txerrors"
	"gitlab.com/getplana/api/utils/update"
)

func Delete(db *sqlx.DB, goalID int) error {
	tx := db.MustBegin()
	var err error
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	err = deleteTasksForGoal(tx, goalID)
	if err != nil {
		err = txerrors.Error("deleting tasks for goal to delete", err)
		return err
	}

	err = shiftUpGoalsAfterGoalToDelete(tx, goalID)
	if err != nil {
		err = txerrors.Error("shifting up goals after goal to delete", err)
		return err
	}

	err = deleteGoal(tx, goalID)
	if err != nil {
		err = txerrors.Error("deleting goal", err)
		return err
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return err
	}

	return nil
}

func deleteTasksForGoal(tx *sqlx.Tx, goalID int) error {
	sql := `DELETE FROM task
		    WHERE goal_id = :goal_id`

	params := map[string]interface{}{
		"goal_id": goalID,
	}

	_, err := tx.NamedExec(sql, params)
	if err != nil {
		return err
	}

	return nil
}

func shiftUpGoalsAfterGoalToDelete(tx *sqlx.Tx, goalID int) error {
	goalToDelete, err := getGoalToDelete(tx, goalID)
	if err != nil {
		err = fmt.Errorf("getting goal to delete: %s", err)
		return err
	}

	goals, err := getGoalsBelowGoalToDelete(tx, *goalToDelete)
	if err != nil {
		err = fmt.Errorf("getting goals below goal to delete: %s", err)
		return err
	}

	for _, goal := range goals {
		newOrderNumber := goal.OrderNumber - 1
		goalUpdate := GoalUpdate{OrderNumber: &newOrderNumber}
		err = update.Update(tx, "goal", goal.GoalID, goalUpdate)
		if err != nil {
			context := fmt.Sprintf("moving goal %d from order number %d to %d", goal.GoalID, goal.OrderNumber, newOrderNumber)
			err = txerrors.Error(context, err)
			return err
		}
	}

	return nil
}

func getGoalToDelete(tx *sqlx.Tx, goalID int) (*GoalToDelete, error) {
	sql := `SELECT customer_id,
				   state,
				   order_number
			FROM goal
			WHERE id = $1`

	goal := GoalToDelete{}
	err := tx.Get(
		&goal,
		sql,
		goalID) // $1
	if err != nil {
		return nil, err
	}

	return &goal, nil
}

func getGoalsBelowGoalToDelete(tx *sqlx.Tx, goalToDelete GoalToDelete) ([]GoalOrder, error) {
	sql := `SELECT id,
				   order_number
			FROM goal
			WHERE customer_id = $1
			AND state = $2::text
			AND order_number > $3`

	goals := []GoalOrder{}
	err := tx.Select(
		&goals,
		sql,
		goalToDelete.CustomerID,  // $1
		goalToDelete.State,       // $2
		goalToDelete.OrderNumber) // $3
	if err != nil {
		return nil, err
	}

	return goals, nil
}

func deleteGoal(tx *sqlx.Tx, goalID int) error {
	sql := `UPDATE goal
			SET state = :state		
			WHERE id = :id`

	params := map[string]interface{}{
		"id":    goalID,
		"state": constants.GoalStateDeleted,
	}

	_, err := tx.NamedExec(sql, params)
	if err != nil {
		return err
	}

	return nil
}
