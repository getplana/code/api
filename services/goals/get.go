package goals

import (
	"github.com/jmoiron/sqlx"
)

func Get(db *sqlx.DB, customerID int, isActive bool) ([]Goal, error) {
	sql := `SELECT g.id,
				   g.text,
				   g.order_number,
				   c.hue,
				   c.saturation,
				   c.lightness
			FROM goal AS g
			INNER JOIN color AS c ON g.color_id = c.id
			WHERE g.customer_id = $1
			AND g.state = $2::text
			ORDER BY order_number`

	goals := []Goal{}
	state := getGoalState(isActive)
	err := db.Select(
		&goals,
		sql,
		customerID, // $1
		state)      // $2
	if err != nil {
		return nil, err
	}

	return goals, nil
}
