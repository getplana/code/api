package goals

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/getplana/api/constants"
	"gitlab.com/getplana/api/utils/dbutil"
	"gitlab.com/getplana/api/utils/pointer"
	"gitlab.com/getplana/api/utils/txerrors"
)

func Create(db *sqlx.DB, newGoal NewGoal) (*Goal, error) {
	if newGoal.CustomerID == nil {
		err := fmt.Errorf("customer ID is missing")
		return nil, err
	}

	numActiveGoals, err := getNumActiveGoals(db, *newGoal.CustomerID)
	if err != nil {
		err = fmt.Errorf("getting number of active goals: %s", err)
		return nil, err
	}

	newGoal.State = pointer.String(constants.GoalStateActive)
	newGoal.OrderNumber = pointer.Int(*numActiveGoals + 1)

	tx := db.MustBegin()
	defer func() {
		err = txerrors.Handle(tx, err)
	}()

	createdGoal, err := createGoal(tx, newGoal)
	if err != nil {
		err = txerrors.Error("creating new goal", err)
		return nil, err
	}

	err = dbutil.CommitTx(tx)
	if err != nil {
		return nil, err
	}

	return createdGoal, nil
}

func getNumActiveGoals(db *sqlx.DB, customerID int) (*int, error) {
	sql := `SELECT COUNT(*)
			FROM goal
			WHERE customer_id = $1
			AND state = $2`

	var num int
	err := db.Get(
		&num,
		sql,
		customerID,                // $1
		constants.GoalStateActive) // $2
	if err != nil {
		return nil, err
	}

	return &num, nil
}

func createGoal(tx *sqlx.Tx, newGoal NewGoal) (*Goal, error) {
	sql := `
	WITH inserted_goal AS (
		INSERT INTO goal (
			customer_id,
			text,
			color_id,
			state,
			order_number
		) VALUES (
			$1,
			$2,
			$3,
			$4,
			$5
		) RETURNING *
	)
	SELECT
		ig.id,
		ig.text,
		ig.order_number,
		c.hue,
		c.saturation,
		c.lightness
	FROM inserted_goal AS ig
	INNER JOIN color AS c ON ig.color_id = c.id`

	createdGoal := Goal{}
	err := tx.Get(
		&createdGoal,
		sql,
		newGoal.CustomerID,  // $1
		newGoal.Text,        // $2
		newGoal.ColorID,     // $3
		newGoal.State,       // $4
		newGoal.OrderNumber) // $5
	if err != nil {
		err = fmt.Errorf("creating new goal: %s", err)
		return nil, err
	}

	return &createdGoal, nil
}
